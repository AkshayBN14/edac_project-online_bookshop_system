import React, { Component } from 'react'
import ApiService from '../../service/ApiService'

class UpdateProfileScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: 0,
      firstName: '',
      lastName: '',
      userName: '',
      email: '',
      password: '',
      contact: '',
      role: '',
      dob: '',
      message: null,
    }
    this.saveUser = this.saveUser.bind(this)
    this.loadUser = this.loadUser.bind(this)
  }

  componentDidMount() {
    console.log('in load User componentDidMount!')
    this.loadUser()
  }

  loadUser() {
    console.log('id: ', this.state.id)
    console.log('firstName: ', this.state.firstName)
    console.log('lastName: ', this.state.lastName)
    console.log('userName: ', this.state.userName)
    console.log('email: ', this.state.email)
    console.log('password: ', this.state.password)
    console.log('contact: ', this.state.contact)
    console.log('role: ', this.state.role)
    console.log('dob: ', this.state.dob)
    console.log('in load book!')
    console.log(
      'userId from sessionStorage',
      window.sessionStorage.getItem('userId')
    )
    console.log('uid from LStorage-->', window.sessionStorage.getItem('userId'))
    ApiService.fetchUserById(window.sessionStorage.getItem('userId'))
      .then((res) => {
        let user = res.data
        console.log(res)
        console.log('-------------------fetchUserById------------------------')
        console.log('id: ', user.id)
        console.log('firstName: ', user.firstName)
        console.log('lastName: ', user.lastName)
        console.log('userName: ', user.userName)
        console.log('email: ', user.email)
        console.log('password: ', user.password)
        console.log('contact: ', user.contact)
        console.log('role: ', user.role)
        console.log('dob: ', user.dob)
        console.log('------------------fetchUserById-----------------------')
        console.log('User details from DB: ', user)
        this.setState({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          userName: user.userName,
          email: user.email,
          password: user.password,
          contact: user.contact,
          role: user.role,
          dob: user.dob,
        })
        console.log(
          '------------------after setting state fetchUserById-----------------------'
        )
        console.log('id: ', this.state.id)
        console.log('firstName: ', this.state.firstName)
        console.log('lastName: ', this.state.lastName)
        console.log('userName: ', this.state.userName)
        console.log('email: ', this.state.email)
        console.log('password: ', this.state.password)
        console.log('contact: ', this.state.contact)
        console.log('role: ', this.state.role)
        console.log('dob: ', this.state.dob)
        console.log(
          '------------------after setting state -----------------------'
        )
      })
      .catch((error) => {
        console.log(
          'in fetchUserById method catch!!!!!!!in EditUserDetailsScreen!!!!!!',
          error
        )
        // console.log('id: ', this.state.id)
        // console.log('firstName: ', this.state.firstName)
        // console.log('lastName: ', this.state.lastName)
        // console.log('userName: ', this.state.userName)
        // console.log('email: ', this.state.email)
        // console.log('contact: ', this.state.contact)
        // console.log('dob: ', this.state.dob)
      })
  }

  saveUser = (e) => {
    // e.preventDefault()
    console.log(e)
    let user = {
      id: this.state.id,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      userName: this.state.userName,
      email: this.state.email,
      password: this.state.password,
      contact: this.state.contact,
      role: this.state.role,
      dob: this.state.dob,
    }
    console.log('-------------------saveUser------------------------')
    console.log('id: ', user.id)
    console.log('firstName: ', user.firstName)
    console.log('lastName: ', user.lastName)
    console.log('userName: ', user.userName)
    console.log('email: ', user.email)
    console.log('password: ', user.password)
    console.log('contact: ', user.contact)
    console.log('dob: ', user.dob)
    console.log('-----------------------------------------')
    console.log('====>', user)
    // debugger
    ApiService.editUser(user)
      .then((res) => {
        // debugger
        console.log('RESPONSE: ', res)
        console.log('user: ', user)
        console.log('user updated successfully')
        this.setState({ message: 'user updated successfully.' })
        // window.sessionStorage.removeItem('userId')
        console.log('before push!!!!!!!!!!!!!!!!!!!!!!!!')
        // const location = '/admin/all-books'
        // <Redirect to='/admin/all-books'/>
        if (user.role == 'CUSTOMER') this.props.history.push('/customerhome')
        else if (user.role == 'ADMIN') this.props.history.push('/adminhome')
      })
      .catch((error) => {
        console.log('in editBook method catch!!!!!!!!!!!!!!!!!!', error)
      })
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  render() {
    return (
      <div>
        <h2 className="text-center">Edit User Details</h2>
        <div>
          <div className="form-group">
            <label>FirstName:</label>
            <input
              type="text"
              placeholder="FirstName"
              name="firstName"
              className="form-control"
              // readonly=""
              defaultValue={this.state.firstName}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Last Name:</label>
            <input
              type="text"
              placeholder="LastName"
              name="lastName"
              className="form-control"
              // readonly=""
              // value={this.state.category}
              defaultValue={this.state.lastName}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>User Name:</label>
            <input
              type="text"
              placeholder="UserName"
              name="userName"
              className="form-control"
              defaultValue={this.state.userName}
              readonly=""
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Email:</label>
            <input
              type="text"
              placeholder="Email"
              name="email"
              className="form-control"
              readonly=""
              defaultValue={this.state.email}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Password:</label>
            <input
              type="password"
              placeholder="Password"
              name="password"
              className="form-control"
              readonly=""
              defaultValue={this.state.password}
              onChange={this.onChange}
            />
          </div>
          <div className="form-floating">
            <div className="form-group">
              <label>Contact:</label>
              <input
                type="text"
                placeholder="Contact"
                name="contact"
                className="form-control"
                defaultValue={this.state.contact}
                onChange={this.onChange}
              />
            </div>
            {/* <label for="floatingTextarea2">Comments</label> */}
          </div>
          <div className="form-group">
            <label>Date of Birth:</label>
            <input
              type="date"
              placeholder="DOB"
              name="dob"
              className="form-control"
              defaultValue={this.state.dob}
              onChange={this.onChange}
            />
          </div>
          <br />
        </div>
        <button className="btn btn-success" onClick={this.saveUser}>
          Save
        </button>
      </div>
    )
  }
}

export default UpdateProfileScreen
