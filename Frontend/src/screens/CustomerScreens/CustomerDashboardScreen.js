// import { Link } from 'react-router-dom'
import { Component, useEffect } from 'react'
// import { useDispatch, useSelector } from 'react-redux'
import ApiService from '../../service/ApiService'
// import AddBookScreen from './AddBookScreen'

class CustomerDashboardScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: [],
      count: 0,
      filterBy: null,
      authors: [],
      categories: [],
      searchTerm: '',
      message: null,
      addressOfCurrentUser: '',
      // book: null,
      cartBooks: [{}], //e keys->userId: ... bookList-> (bid, price, qty)
      // e.g.
      // {
      //  userId: id
      //   "cartBooks": [
      //       {
      //           "bookId": 2,
      //           "qty": 1,
      //           "price": 390
      //       },
      //       {
      //           "bookId": 1,
      //           "qty": 2,
      //           "price": 350
      //       }
      //   ]
      // }
    }
    this.deleteBookFn = this.deleteBookFn.bind(this)
    this.editBook = this.editBook.bind(this)
    this.addBook = this.addBook.bind(this)
    this.reloadAllBooksList = this.reloadAllBooksList.bind(this)
  }

  componentDidMount() {
    if (this.state.filterBy == null) this.reloadAllBooksList()
    // else if (this.state.filterBy == 'Category') this.reloadBooksListByCategory()
    // else if (this.state.filterBy == 'Author') this.reloadBooksListByAuthor()
  }

  reloadAllBooksList() {
    console.log('**********++++++++************')
    ApiService.getAllBooks().then((response) => {
      console.log(
        '======================------in reloadBooksList-------------=============='
      )
      this.setState({ books: response.data })
      console.log('All Books: ' + response.data)
      console.log('All Books: ', this.state.books)
    })
    ApiService.getAllCategories().then((response) => {
      console.log('in reloadBooksListByCategory')
      this.setState({ categories: response.data })
      console.log('All Books: ' + response.data)
      console.log('All Books: ', this.state.categories)
    })
    ApiService.getAllAuthors().then((response) => {
      console.log('in reloadBooksListByCategory')
      this.setState({ authors: response.data })
      console.log('All Books: ' + response.data)
      console.log('All Books: ', this.state.authors)
    })
  }

  reloadBooksListByCategory() {
    this.setState({
      books: this.state.books.filter((book) =>
        book.category
          .toLowerCase()
          .includes(this.state.searchTerm.toLowerCase())
      ),
    })
    // this.state.books.filter((book) =>
    //     book.category.toLowerCase().includes(this.state.searchTerm.toLowerCase())
  }
  reloadBooksListByAuthor() {
    this.setState({
      books: this.state.books.filter((book) =>
        book.author.toLowerCase().includes(this.state.searchTerm.toLowerCase())
      ),
    })
  }

  // reloadAddressOfCurrentUser() {
  //   this.setState({
  //     addressOfCurrentUser:
  //     ),
  //   })
  // }

  editSearchTerm = (e) => {
    console.log(e.target.value)
    this.setState({ [e.target.name]: e.target.value })
  }

  // dynamicSearch = () => {
  //   if (this.state.filterBy == 'Category') {
  //     return this.state.books.filter((book) =>
  //       book.category
  //         .toLowerCase()
  //         .includes(this.state.searchTerm.toLowerCase())
  //     )
  //   }
  //   if (this.state.filterBy == 'Author') {
  //     return this.state.books.filter((book) =>
  //       book.author.toLowerCase().includes(this.state.searchTerm.toLowerCase())
  //     )
  //   }
  // }

  deleteBookFn(bookId) {
    // e.preventDefault()
    // let bookId = {
    //   bookId: this.state.bookId,
    // }
    // const header = {
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    // }
    const body = {
      bookId,
    }
    ApiService.deleteBook(body).then((res) => {
      console.log('RESPONSE:', res)
      this.setState({ message: 'Book deleted successfully.' })
      this.setState({
        books: this.state.books.filter((book) => book.id !== bookId),
      })
    })
  }

  editBook(bookId) {
    window.sessionStorage.setItem('bookId', bookId)
    this.props.history.push('/admin/edit-book')
  }

  addBook() {
    window.sessionStorage.removeItem('bookId')
    this.props.history.push('/admin/add-book')
  }

  addToCart(book) {
    // add to cart
    // take a array for cart in state of this class
    // after clicking on add to cart button fetch and set bookId, qty, price in a book object and add it to the cart array
    // store the cart in localStorage
    // during viewing Cart get the cart from localStorage and display all book details
    // place order will be available on another screen (viewCart)
    if (window.localStorage.getItem('cartBooks') != null) {
      console.log('---in if---')
      //const cartttt = {'userId': window.localStorage.getItem('USERID'), cartBooks: this.state.cart }
      const uid = window.sessionStorage.getItem('USERID')
      console.log('USERID: ', uid)
      this.setState({
        cartBooks: JSON.parse(window.localStorage.getItem('cartBooks')),
      })
      console.log('CART////: ', this.state.cartBooks)
      this.state.cartBooks.push({
        // cartBooks is a array in array(cart)
        bookId: book.id,
        qty: 1,
        price: book.price,
      })
      console.log('CART$$$$: ', this.state.cartBooks)
      window.localStorage.setItem(
        'cartBooks',
        JSON.stringify(this.state.cartBooks)
      )
      // this.props.history({
      //   pathname: `/customer/view-cart`,
      //   // search: searchString,
      //   target: '_blank',
      // })
      // const url = '/customer/view-cart'
      // window.open(url)
      this.props.history.push('/customer/view-cart')
    } else {
      //control will come here only once at first during first time adding a book to cart otherwise the control will always go in above if condition where cart will be present in localStorage
      console.log('---in else---')
      const uid = window.sessionStorage.getItem('USERID')
      console.log('CART---: ', this.state.cartBooks)
      console.log('USERID: ', uid)
      console.log('CART***: ', this.state.cartBooks)
      this.state.cartBooks.push({
        bookId: book.id,
        qty: 1,
        price: book.price,
      })
      console.log('CART&&&&: ', this.state.cartBooks)
      window.localStorage.setItem(
        'cartBooks',
        JSON.stringify(this.state.cartBooks)
      )
      // this.props.history({
      //   pathname: `/customer/view-cart`,
      //   // search: searchString,
      //   target: '_blank',
      // })
      // const url = '/customer/view-cart'
      // window.open(url)
      this.props.history.push('/customer/view-cart')
    }
  }

  //style={{ scrollBehavior: 'auto' }}
  //style="width: 18rem;
  render() {
    return (
      <div>
        <h2 className="text-center">Books Gallery</h2>
        {/* <button
          className="btn btn-primary"
          style={{ width: '100px' }}
          onClick={() => this.addBook()}>
          {' '}
          Add Book
        </button> */}
        <div>
          <select
            className="dropdown"
            style={{ width: 200, marginLeft: 400 }}
            id="filter"
            onChange={(e) => {
              console.log('in onChange of select/filter tag', e.target.value)
              this.setState({ filterBy: e.target.value })
              // reloadOrdersList()
            }}>
            <option>FilterBy</option>
            <option value="Author">Author</option>
            <option value="Category">Category</option>
            {/* <option value="Price">Price</option> TODO */}
          </select>
          <br />
          <div class="input-group" style={{ marginLeft: 400 }}>
            <div class="form-outline">
              <input
                style={{ width: 200, height: 35 }}
                type="search"
                id="form1"
                name="searchTerm"
                class="form-control"
                onChange={this.editSearchTerm}
              />
              <label class="form-label" for="form1">
                Search
              </label>
            </div>
            <div className="float-end">
              <button
                onClick={this.componentDidMount}
                style={{ width: 50, height: 35 }}
                type="button"
                class="btn btn-success">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </div>
        {/* <div>{sessionStorage['msg']}</div> */}
        <table
          style={{ alignContent: 'center', marginLeft: -80, width: 900 }}
          className="table table-striped">
          {/* <thead>
            <tr> */}
          {/* <th hidden={this.state.id}>Id</th> not reqd!*/}
          {/* <th className="hidden">Id</th>  not reqd!*/}
          {/* <th>Author</th>
              <th>BookName</th>
              <th>Category</th> */}
          {/* <th>Copies</th> not reqd!*/}
          {/* <th>Description</th>
              <th>Price</th>
              <th>Stock</th>
            </tr> 
          </thead>*/}
          <tbody>
            {this.state.books.map((book) => (
              // key={book.id}>
              <div>
                <td>
                  {/* {(this.state.count = this.state.count + 1)} */}
                  {/* {this.state.count} */}
                  <td>
                    <div className="card" style={{ width: 700 }}>
                      <div className="card-body">
                        <h5 className="card-title">{book.bookName}</h5>
                        <p className="card-text">
                          Author: {book.author}
                          <br />
                          Category: {book.category}
                          <br />
                          Description: {book.description}
                          <br />
                          Price: {book.price}
                          <br />
                          Stock: {book.copies ? 'In Stock' : 'Out of Stock'}
                          <br />
                          <button
                            disabled={!book.copies} // no books -> book.copies=false ***** !book.copies=true
                            className="float-end btn btn-primary"
                            onClick={() => this.addToCart(book)}
                            style={{ width: 120 }}>
                            {' '}
                            Add to Cart
                          </button>
                        </p>
                        {/* <a
                          href="/admin/all-books"
                          className="btn btn-primary my-20">
                          Go to Books Section
                        </a> */}
                      </div>
                    </div>
                  </td>
                  {/* <tr> */}
                  {/* <td>{book.author}</td>
                  <td>{book.bookName}</td>
                  <td>{book.category}</td> */}
                  {/* <td>{book.copies}</td> not reqd!!*/}
                  {/* <td>{book.description}</td>
                  <td>{book.price}</td>
                  <td>{book.copies ? 'In Stock' : 'Out of Stock'}</td>
                  <td>
                    <button
                      disabled={!book.copies} // no books -> book.copies=false ***** !book.copies=true
                      className="btn btn-primary"
                      onClick={() => this.addToCart(book.id)}
                      style={{ width: 120 }}>
                      {' '}
                      Add to Cart
                    </button>
                  </td> */}
                  {/* <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => this.deleteBookFn(book.id)}
                    style={{ width: 70 }}>
                    {' '}
                    Delete
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => this.editBook(book.id)}
                    style={{ marginLeft: '20px', width: 70 }}>
                    {' '}
                    Edit
                  </button>
                </td> */}
                  {/* </tr> */}
                </td>
                {/* {this.state.count % 3 == 0 && <br />} */}
              </div>
              // condition
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default CustomerDashboardScreen
