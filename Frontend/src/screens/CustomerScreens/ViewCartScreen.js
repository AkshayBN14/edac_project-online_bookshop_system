// import { Link } from 'react-router-dom'
import { Component, useEffect } from 'react'
// import { useDispatch, useSelector } from 'react-redux'
import ApiService from '../../service/ApiService'
// import AddBookScreen from './AddBookScreen'

class ViewCart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: [],
      cartBooks: [],
      temp: [],
      message: null,
    }
    this.reloadBooksList = this.reloadBooksList.bind(this)
  }

  componentDidMount() {
    this.reloadBooksList()
  }

  reloadBooksList() {
    ApiService.getAllBooks()
      .then((response) => {
        // debugger
        console.log('All Books: ' + response.data)
        // debugger
        this.setState({ books: response.data })
        console.log('All Books: ' + response.data)
        // debugger
        console.log('All Books: ', this.state.books)
      })
      .catch((e) => {
        console.log('in catch of reloadBooksList')
      })
    // console.log('All Books: ' + response.data)
    this.state.cartBooks = JSON.parse(window.localStorage.getItem('cartBooks'))
    console.log('***********cartBooks************', this.state.cartBooks)
    // employee.employees.filter(({ id }) => !employeeIds.includes(id));
    this.setState({
      temp: this.state.cartBooks.map((c) => {
        this.state.temp.push(this.state.books.find((b) => b.id == c.bookId))
      }),
    })
    console.log('temp', this.state.temp)
    // debugger
    // this.setState({
    //   temp,
    // })
    console.log('^^^^^^^^^^^cartBooks^^^^^^^^^^^', this.state.cartBooks)
    console.log('++++++++++books++++++++++++', this.state.books)
  }

  // submit(e) {
  //   e.preventDefault()
  //   this.state.cartBooks = JSON.parse(window.localStorage.getItem('cartBooks'))
  //   console.log('cartBooks-->', cartBooks)
  //   // console.log(this.refs.name.value, this.refs.price.value)
  //   // this.props.handleProduct(product)
  //   // this.refs.name.value = ''
  //   // this.refs.price.value = 0
  //   // this.refs.info.value = ''
  // }

  render() {
    return (
      <div>
        {/* <form onSubmit={this.submit.bind(this)}> */}
        <h3>Your Cart</h3>
        {/* <div className="row form-group">
          <label className="col-sm-2  col-sm-form-label">name:</label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control"
              ref="name"
              placeholder="e.g.) android"
              required
            />
          </div>
        </div> */}

        {/* <div className="row form-group">
          <label className="col-sm-2  col-sm-form-label">price:</label>
          <div className="col-sm-10">
            <input
              type="number"
              className="form-control"
              ref="price"
              placeholder="e.g.) 100"
              required
            />
          </div>
        </div>

        <div className="row form-group">
          <label className="col-sm-2  col-sm-form-label">info:</label>
          <div className="col-sm-10">
            <input
              type="text"
              className="form-control"
              ref="info"
              placeholder="e.g.) product of google"
            />
          </div>
        </div> */}
        {this.state.temp.map((book) => (
          // key={book.id}>
          <div>
            {/* {(this.state.count = this.state.count + 1)} */}
            {/* {this.state.count} */}
            <td>
              <div className="card" style={{ width: 700 }}>
                <div className="card-body">
                  <h5 className="card-title">{this.state.temp.bookName}</h5>
                  <p className="card-text">
                    Author: {this.state.temp.author}
                    <br />
                    Category: {this.state.temp.category}
                    <br />
                    Description: {this.state.temp.description}
                    <br />
                    Price: {this.state.temp.price}
                    <br />
                    {/* Stock: {book.copies ? 'In Stock' : 'Out of Stock'}
                      <br /> */}
                    {/* <button
                        disabled={!book.copies} // no books -> book.copies=false ***** !book.copies=true
                        className="float-end btn btn-primary"
                        onClick={() => this.addToCart(book)}
                        style={{ width: 120 }}>
                        {' '}
                        Add to Cart
                      </button> */}
                  </p>
                </div>
              </div>
            </td>
          </div>
        ))}

        <div className="row form-group">
          <div className="offset-2 col-10">
            <button className="btn btn-outline-primary">Place Order</button>
          </div>
        </div>

        <hr />
      </div>
    )
  }
}

export default ViewCart
