import Header from '../components/Header'

const AboutScreen = (props) => {
  return (
    <div>
      <h3>
        <Header title="" />
      </h3>
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">About Us</h5>
          <p className="card-text">
            Dear readers, We offer huge collection of books in diverse category
            of Fiction, Non-fiction, Biographies, History, Religions, Self
            -Help, Children. We also sell in vast collection of Investments and
            Management, Computers, Engineering, Medical, College and School text
            references books proposed by different institutes as syllabus across
            the country. Besides to this, we also offer a large collection of
            E-Books at very fair pricing. <br />
            We attempt to extend the customer satisfaction by catering easy
            user-friendly search engine, quick and user-friendly payment options
            and quicker delivery systems. Upside to all of this, we are disposed
            to provide exciting offers and pleasant discounts on our books.
            <br />
            As well, we humbly invite all the sellers around the country to
            partner with us.{' '}
            {/*We will surely provide you the platform for many
            sparkling domains and grow the “BooksWagon” family.We would like to
            thank you for shopping with us. You can write us for any new
            thoughts at “email-id” helping us to improvise for the reader
            satisfaction. */}
          </p>
          <a href="/admin/books" className="btn btn-primary">
            Go to
          </a>
        </div>
      </div>
    </div>
  )
}

export default AboutScreen
