import React, { Component } from 'react'
import ApiService from '../../service/ApiService'

class EditBookScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: 0,
      author: '',
      // book_img_file_name
      bookName: '',
      category: '',
      copies: 0,
      description: '',
      price: 0,
      message: null,
    }
    this.saveBook = this.saveBook.bind(this)
    this.loadBook = this.loadBook.bind(this)
  }

  componentDidMount() {
    console.log('in load book componentDidMount!')
    this.loadBook()
  }

  loadBook() {
    console.log('bookid: ', this.state.id)
    console.log('author: ', this.state.author)
    console.log('bookName: ', this.state.bookName)
    console.log('category: ', this.state.category)
    console.log('copies: ', this.state.copies)
    console.log('desc: ', this.state.description)
    console.log('price: ', this.state.price)
    console.log('in load book!')
    console.log(
      'bookId from sessionStorage',
      window.sessionStorage.getItem('bookId')
    )
    ApiService.fetchBookById(window.sessionStorage.getItem('bookId'))
      .then((res) => {
        let book = res.data
        console.log(res)
        console.log('-------------------fetchBookById------------------------')
        console.log('bookid: ', book.id)
        console.log('author: ', book.author)
        console.log('bookName: ', book.bookName)
        console.log('category: ', book.category)
        console.log('copies: ', book.copies)
        console.log('desc: ', book.description)
        console.log('price: ', book.price)
        console.log('------------------fetchBookById-----------------------')
        console.log('Book details from DB: ', book)
        this.setState({
          id: book.id,
          author: book.author,
          bookName: book.bookName,
          category: book.category,
          copies: book.copies,
          description: book.description,
          price: book.price,
        })
        console.log(
          '------------------after setting state fetchBookById-----------------------'
        )
        console.log('bookid: ', this.state.id)
        console.log('author: ', this.state.author)
        console.log('bookName: ', this.state.bookName)
        console.log('category: ', this.state.category)
        console.log('copies: ', this.state.copies)
        console.log('desc: ', this.state.description)
        console.log('price: ', this.state.price)
        console.log(
          '------------------after setting state fetchBookById-----------------------'
        )
      })
      .catch((error) => {
        console.log(
          'in fetchBookById method catch!!!!!!!in EditBookScreen!!!!!!',
          error
        )
        console.log('author: ', this.state.author)
        console.log('bookName: ', this.state.bookName)
        console.log('category: ', this.state.category)
        console.log('copies: ', this.state.copies)
        console.log('desc: ', this.state.description)
        console.log('price: ', this.state.price)
      })
  }

  onChange = (e) => {
    // console.log(e.target.name + ' b-4 ' + e.target.value)
    this.setState({ [e.target.name]: e.target.value })
    // console.log(e.target.name + ' a-ft ' + e.target.value)
  }

  saveBook = (e) => {
    // e.preventDefault()
    console.log(e)
    let book = {
      id: this.state.id,
      author: this.state.author,
      bookName: this.state.bookName,
      category: this.state.category,
      copies: this.state.copies,
      description: this.state.description,
      price: this.state.price,
    }
    console.log('-------------------saveBook------------------------')
    console.log('bookid: ', book.id)
    console.log('author: ', book.author)
    console.log('bookName: ', book.bookName)
    console.log('category: ', book.category)
    console.log('copies: ', book.copies)
    console.log('desc: ', book.description)
    console.log('price: ', book.price)
    console.log('------------------saveBook-----------------------')
    console.log(book)
    debugger
    ApiService.editBook(book)
      .then((res) => {
        debugger
        console.log('RESPONSE: ', res)
        console.log('BOOK: ', book)
        console.log('Book updated successfully')
        this.setState({ message: 'Book updated successfully.' })
        window.sessionStorage.removeItem('bookId')
        console.log('before push!!!!!!!!!!!!!!!!!!!!!!!!')
        // const location = '/admin/all-books'
        // <Redirect to='/admin/all-books'/>
        this.props.history.push('/admin/all-books')
      })
      .catch((error) => {
        console.log('in editBook method catch!!!!!!!!!!!!!!!!!!', error)
      })
  }

  render() {
    return (
      <div>
        <h2 className="text-center">Edit Book</h2>
        <div>
          <div className="form-group">
            <label>Author:</label>
            <input
              type="text"
              placeholder="Author"
              name="author"
              className="form-control"
              // readonly="true"
              defaultValue={this.state.author}
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Book Name:</label>
            <input
              type="text"
              placeholder="BookName"
              name="bookName"
              className="form-control"
              readonly=""
              // value={this.state.category}
              defaultValue={this.state.bookName}
            />
          </div>
          <div className="form-group">
            <label>Category:</label>
            <input
              placeholder="Category"
              name="category"
              className="form-control"
              // value={this.state.category}
              defaultValue={this.state.category}
              readonly=""
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label>Copies:</label>
            <input
              type="number"
              placeholder="Copies"
              name="copies"
              className="form-control"
              // value={this.state.copies}
              defaultValue={this.state.copies}
              onChange={this.onChange}
            />
          </div>
          <div className="form-floating">
            <div className="form-group">
              <label>Description:</label>
              <textarea
                required=""
                height="auto"
                className="form-control"
                placeholder="Description"
                id="desc"
                name="description"
                // value={this.state.description}
                defaultValue={this.state.description}
                onChange={this.onChange}
                style={{ height: 150 }}
              />
            </div>
            {/* <label for="floatingTextarea2">Comments</label> */}
          </div>
          <div className="form-group">
            <label>Price:</label>
            <input
              type="number"
              placeholder="Price"
              name="price"
              className="form-control"
              defaultValue={this.state.price}
              onChange={this.onChange}
            />
          </div>
          <br />
        </div>
        <button className="btn btn-success" onClick={this.saveBook}>
          Save
        </button>
      </div>
    )
  }
}

export default EditBookScreen
