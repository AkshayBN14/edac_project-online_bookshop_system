import { Link } from 'react-router-dom'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const AdminDashboardScreen = (props) => {
  // const cartItemsStore = useSelector((store) => store.cartItemsStore)
  // const { response, loading, error } = cartItemsStore

  // const dispatch = useDispatch()

  // useEffect(() => {
  //   console.log('in use effect')
  //   dispatch(getAllCartItems())
  // }, [])

  //style="width: 18rem;
  return (
    <div>
      Admin Dashboard
      <div className="container">
        {/* //used bootstrap CARD instead of just a Link tag */}
        <div className="row">
          <div className="card col">
            <div className="card-body">
              <h5 className="card-title">Books Section</h5>
              <p className="card-text">
                Provides facility to Add a Book, Update a Book, Delete a Book!
              </p>
              <div className="card-footer">
                <a
                  href="/admin/all-books"
                  className="btn btn-primary card-footer">
                  Go to Books Section
                </a>
              </div>
            </div>
          </div>

          <div className="card col">
            <div className="card-body">
              <h5 className="card-title">Orders Section</h5>
              <p className="card-text">
                Provides facility to View all Orders by Customers by
                Date/Customer!
              </p>
              <a
                href="/admin/orders"
                className="btn btn-primary"
                style={{ position: '' }}>
                Go to Orders Section
              </a>
            </div>
          </div>
          {/* </div> */}
          {/* <div>
          <Link to="/admin/books">Books</Link>
          </div> */}
          {/* <br /> */}
          {/* <div className="row"> */}
          <div className="card col">
            <div className="card-body">
              <h5 className="card-title">Customers Section</h5>
              <p className="card-text">
                Provides facility to Add a Customer, Delete/Suspend a Customer,
                View Customers List, Get Customer by Email!
              </p>
              <a href="/admin/customers" className="btn btn-primary">
                Go to Customers Section
              </a>
            </div>
          </div>
        </div>
        {/* <div>
          <Link to="/admin/customers">Customer</Link>
          </div> */}
      </div>
      {/* <div>
        <Link to="/admin/orders">Orders</Link>
      </div> */}
    </div>
  )
}

export default AdminDashboardScreen
