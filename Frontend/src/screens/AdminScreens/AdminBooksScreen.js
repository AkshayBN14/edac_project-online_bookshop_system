import { Link } from 'react-router-dom'
import { Component, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ApiService from '../../service/ApiService'
import AddBookScreen from './AddBookScreen'

class AdminBooksScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: [],
      message: null,
    }
    this.deleteBookFn = this.deleteBookFn.bind(this)
    this.editBook = this.editBook.bind(this)
    this.addBook = this.addBook.bind(this)
    this.reloadBooksList = this.reloadBooksList.bind(this)
  }

  componentDidMount() {
    this.reloadBooksList()
  }

  reloadBooksList() {
    ApiService.getAllBooks().then((response) => {
      console.log(
        '======================------in reloadBooksList-------------=============='
      )
      this.setState({ books: response.data })
      console.log('All Books: ' + response.data)
      console.log('All Books: ', this.state.books)
    })
  }

  deleteBookFn(bookId) {
    // e.preventDefault()
    // let bookId = {
    //   bookId: this.state.bookId,
    // }
    // const header = {
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    // }
    const body = {
      bookId,
    }
    ApiService.deleteBook(body).then((res) => {
      console.log('RESPONSE:', res)
      this.setState({ message: 'Book deleted successfully.' })
      this.setState({
        books: this.state.books.filter((book) => book.id !== bookId),
      })
    })
  }

  editBook(bookId) {
    window.sessionStorage.setItem('bookId', bookId)
    this.props.history.push('/admin/edit-book')
  }

  addBook() {
    window.sessionStorage.removeItem('bookId')
    this.props.history.push('/admin/add-book')
  }

  //style="width: 18rem;
  render() {
    return (
      <div>
        <h2 className="text-center">All Books Details</h2>
        <button
          className="btn btn-primary"
          style={{ width: '100px' }}
          onClick={() => this.addBook()}>
          {' '}
          Add Book
        </button>
        {/* <div>{sessionStorage['msg']}</div> */}
        <table className="table table-striped">
          <thead>
            <tr>
              {/* <th hidden={this.state.id}>Id</th> */}
              {/* <th className="hidden">Id</th> */}
              <th>Author</th>
              <th>BookName</th>
              <th>Category</th>
              <th>Copies</th>
              <th>Description</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.books.map((book) => (
              // key={book.id}>
              <tr>
                <td>{book.author}</td>
                <td>{book.bookName}</td>
                <td>{book.category}</td>
                <td>{book.copies}</td>
                <td>{book.description}</td>
                <td>{book.price}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => this.deleteBookFn(book.id)}
                    style={{ width: 70 }}>
                    {' '}
                    Delete
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => this.editBook(book.id)}
                    style={{ marginLeft: '20px', width: 70 }}>
                    {' '}
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default AdminBooksScreen
