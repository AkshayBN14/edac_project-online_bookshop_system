import React, { Component } from 'react'
import ApiService from '../../service/ApiService'

class AddBookScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      author: '',
      // book_img_file_name
      bookName: '',
      category: '', //drop down
      copies: 0,
      description: '',
      price: '',
      message: null,
    }
    this.saveBook = this.saveBook.bind(this)
  }

  saveBook = (e) => {
    e.preventDefault()
    let book = {
      author: this.state.author,
      bookName: this.state.bookName,
      category: this.state.category,
      copies: this.state.copies,
      description: this.state.description,
      price: this.state.price,
    }
    console.log(book)
    ApiService.addBook(book).then((res) => {
      console.log(res)
      this.setState({ message: 'Book added successfully.' })
      this.props.history.push('/admin/all-book')
    })
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value })

  render() {
    return (
      <div>
        <h2 className="text-center">Add Book</h2>
        <form>
          <div className="form-group">
            <label>Author:</label>
            <input
              type="text"
              placeholder="Author"
              name="author"
              className="form-control"
              value={this.state.author}
              onChange={this.onChange}
              required=""
            />
          </div>
          <br />
          <div className="form-group">
            <label>Book Name:</label>
            <input
              type="text"
              placeholder="BookName"
              name="bookName"
              className="form-control"
              value={this.state.bookName}
              onChange={this.onChange}
              required=""
            />
          </div>
          <br />
          {/* NOVEL,COMIC,ACTION,TECHNICAL,FICTION */}
          <div className="form-group">
            <label>Category:</label>
            <div>
              <select
                required=""
                className="dropdown"
                id="category"
                onChange={(e) => {
                  this.setState({ category: e.target.value })
                }}>
                <option>Select category</option>
                <option value="NOVEL">NOVEL</option>
                <option value="COMIC">COMIC</option>
                <option value="ACTION">ACTION</option>
                <option value="TECHNICAL">TECHNICAL</option>
                <option value="FICTION">FICTION</option>
              </select>
              <br />
              {/* <button
                class="btn btn-primary dropdown-toggle"
                type="button"
                id="dropdown1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                value={this.state.category}
                onChange={this.onChange}>
                Category{' '}
              </button> */}
              {/* <ul class="dropdown-menu">
                <li>
                  <a class="dropdown-item">NOVEL</a>
                </li>
                <li>
                  <a class="dropdown-item">COMIC</a>
                </li>
                <li>
                  <a class="dropdown-item">ACTION</a>
                </li>
                <li>
                  <a class="dropdown-item">TECHNICAL</a>
                </li>
                <li>
                  <a class="dropdown-item">FICTION</a>
                </li>
              </ul> */}
            </div>
          </div>
          {/* <div className="form-group">
            <label>Category:</label>
            <input
              type="text"
              placeholder="Category"
              name="category"
              className="form-control"
              value={this.state.category}
              onChange={this.onChange}
            />
          </div> */}
          <br />
          <div className="form-group">
            <label>Copies:</label>
            <input
              required=""
              type="number"
              placeholder="Copies"
              name="copies"
              className="form-control"
              value={this.state.copies}
              onChange={this.onChange}
            />
          </div>
          <br />
          <div className="form-floating">
            <div className="form-group">
              <label>Description:</label>
              <textarea
                required=""
                height="auto"
                className="form-control"
                placeholder="Description"
                id="floatingTextarea2"
                name="description"
                value={this.state.description}
                onChange={this.onChange}
                style={{ height: 150 }}
              />
            </div>
            {/* <label for="floatingTextarea2">Comments</label> */}
          </div>
          {/* <div className="form-group">
            <label>Description:</label>
            <input
              type="text-area"
              placeholder="Description"
              name="description"
            />
          </div> */}
          <br />
          <div className="form-group">
            <label>Price:</label>
            <input
              required=""
              type="number"
              placeholder="Price"
              name="price"
              className="form-control"
              value={this.state.price}
              onChange={this.onChange}
            />
          </div>
          <br />
          <button className="btn btn-success" onClick={this.saveBook}>
            Save
          </button>
        </form>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    )
  }
}

export default AddBookScreen
