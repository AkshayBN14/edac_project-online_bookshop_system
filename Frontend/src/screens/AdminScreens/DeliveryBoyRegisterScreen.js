import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Header from '../../components/Header'
import { signup } from '../../actions/userActions'
import { useDispatch, useSelector } from 'react-redux'

const SignupScreen = (props) => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [userName, setUserName] = useState('')
  const [contact, setContact] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [role, setRole] = useState('DELIVERY_BOY')
  const [dob, setDob] = useState('')
  // const [status, setStatus] = useState(1)

  const dispatch = useDispatch()

  const userSignup = useSelector((store) => store.userSignup)
  const { loading, response, error } = userSignup //obj destructuring

  // listen on change of the [loading, response, error] values
  // similar to ComponentDidMount() ?!
  useEffect(() => {
    console.log('use effect called: ')
    console.log('loading: ', loading)
    console.log('response: ', response)
    console.log('error: ', error)

    // if (response && response.status === 'success') {
    //   props.history.push('/signin')
    // } else if (response && response.status === 'error') {
    //     console.log("here");
    //   alert('User Already Exist, Please login or try with another email')
    // } else if (error) {
    //   console.log("here");
    //   alert('User Already Exist, Please login or try with another email')
    // }
    if (response) {
      // user successfully got registered
      console.log('in condition for response status as success!')
      console.log('RESPONSE: ', response)
      console.log('RESPONSE.STATUS: ', response.status)
      props.history.push('/signin')
    } else if (error) {
      // there is an error while making the API call
      console.log(error)
      alert('Please enter valid details! and make sure every field is filled!')
    }
  }, [loading, response, error])

  const onSignup = () => {
    console.log('first name: ', firstName)
    console.log('last name: ', lastName)
    console.log('userName: ', userName)
    console.log('contact: ', contact)
    console.log('email: ', email)
    console.log('password: ', password)
    console.log('role: ', role)
    console.log('dob: ', dob)
    dispatch(
      signup(firstName, lastName, userName, contact, email, password, role, dob)
    )
  }

  return (
    <div>
      <Header title="Register a DeliveryBoy" />
      <div className="form">
        <div className="mb-3">
          <label className="form-label">First Name</label>
          <input
            onChange={(e) => {
              setFirstName(e.target.value)
            }}
            className="form-control"
            required=""></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Last Name</label>
          <input
            onChange={(e) => {
              setLastName(e.target.value)
            }}
            className="form-control"
            required=""></input>
        </div>
        <div className="mb-3">
          <label className="form-label">User Name</label>
          <input
            onChange={(e) => {
              setUserName(e.target.value)
            }}
            className="form-control"
            required=""></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Contact</label>
          <input
            onChange={(e) => {
              setContact(e.target.value)
            }}
            className="form-control"
            required=""></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input
            onChange={(e) => {
              setEmail(e.target.value)
            }}
            type="email"
            className="form-control"
            placeholder="test@test.com"
            required=""
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password</label>
          <input
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            type="password"
            className="form-control"
            placeholder="*****"
            required=""
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Date Of Birth</label>
          <input
            onChange={(e) => {
              setDob(e.target.value)
            }}
            type="date"
            className="form-control"
            required=""
          />
        </div>
        {/* <div
          onChange={(e) => {
            setRole(e.target.value)
          }}> */}
        {/* <div className="mb-3">
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="role"
                id="R1"
                value="CUSTOMER"
              />
              <label className="form-check-label" for="R1">
                As a Customer
              </label>
            </div>
          </div> */}

        {/* <div className="mb-3">
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="role"
                id="R2"
                value="DELIVERY_BOY"
              />
              <label className="form-check-label" for="R2">
                As a DeliveryBoy
              </label>
            </div>
          </div> */}
        {/* </div> */}

        <div className="mb-3">
          <Link
            to="/admin/dashboard"
            onClick={onSignup}
            className="btn btn-success">
            Signup
          </Link>
          {/* <div className="float-end">
            Existing user? <Link to="/signin">Signin here</Link>
          </div> */}
        </div>
      </div>

      {userSignup.loading && <div>waiting for result</div>}
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  )
}

export default SignupScreen
