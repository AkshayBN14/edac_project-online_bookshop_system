import React, { Component } from 'react'
import ApiService from '../../service/ApiService'

class ListCustomersComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: [],
      message: null,
    }
    this.deleteUser = this.deleteUser.bind(this)
    this.editUser = this.editUser.bind(this)
    this.addUser = this.addUser.bind(this)
    this.reloadUserList = this.reloadUserList.bind(this)
  }

  componentDidMount() {
    this.reloadUserList()
  }

  reloadUserList() {
    ApiService.fetchAllCustomers().then((response) => {
      console.log(
        '======================------in reloadUsersList-------------=============='
      )
      console.log('------>', response)
      this.setState({ users: response.data })
      console.log('response-> ', response.data)
      console.log(this.state.users)
    })
    // UserService.getUsers().then(resp => {
    //     this.setState({ users: resp.data });
    //     console.log(this.state.users);
    // })
  }

  deleteUser(userId) {
    ApiService.deleteUser(userId).then((res) => {
      console.log('RESPONSE:', res)
      this.setState({ message: 'User deleted successfully.' })
      this.setState({
        users: this.state.users.filter((user) => user.id !== userId),
      })
    })
  }

  editUser(id) {
    window.sessionStorage.setItem('userId', id)
    console.log('--------in editUser')
    this.props.history.push('/admin/edit-user') //TODO
  }

  addUser() {
    window.sessionStorage.removeItem('userId')
    this.props.history.push('/signup-customer') //TODO
  }

  render() {
    return (
      <div style={{ marginLeft: -180 }}>
        <h2 className="text-center" style={{ marginLeft: 30 }}>
          Customers List
        </h2>
        <button
          className="btn btn-primary"
          style={{ width: '150px' }}
          onClick={() => this.addUser()}>
          {' '}
          Add Customer
        </button>
        <table className="table table-striped" style={{ width: 850 }}>
          <thead>
            <tr>
              {/* <th className="hidden">Id</th> */}
              <th>FirstName</th>
              <th>LastName</th>
              <th>UserName</th>
              <th>Email</th>
              <th>Date of Birth</th>
              <th>Contact</th>
            </tr>
          </thead>
          <tbody>
            {this.state.users.map((user) => (
              //  key={user.id}
              <tr>
                <td>{user.firstName}</td>
                <td>{user.lastName}</td>
                <td>{user.userName}</td>
                <td>{user.email}</td>
                <td>{user.dob}</td>
                <td>{user.contact}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => this.deleteUser(user.id)}
                    style={{ width: 70 }}>
                    {' '}
                    Delete
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => this.editUser(user.id)}
                    style={{ marginLeft: '20px' }}>
                    {' '}
                    Edit
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default ListCustomersComponent
