import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { signup } from '../actions/userActions'
import { useDispatch, useSelector } from 'react-redux'

const SignupScreen = (props) => {
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [userName, setUserName] = useState('')
  const [contact, setContact] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [dob, setDob] = useState('')

  const dispatch = useDispatch()

  const userSignup = useSelector((store) => store.userSignup)
  const { loading, response, error } = userSignup //obj destructuring

  // listen on change of the [loading, response, error] values
  // similar to ComponentDidMount() ?!
  useEffect(() => {
    console.log('use effect called: ')
    console.log('loading: ', loading)
    console.log('response: ', response)
    console.log('error: ', error)

    // if (response && response.status === 'success') {
    //   props.history.push('/signin')
    // } else if (response && response.status === 'error') {
    //     console.log("here");
    //   alert('User Already Exist, Please login or try with another email')
    // } else if (error) {
    //   console.log("here");
    //   alert('User Already Exist, Please login or try with another email')
    // }

    if (response && response.status == 'success') {
      // user successfully got registered
      console.log(response)
      console.log(response.status)
      props.history.push('/signin')
    } else if (error) {
      // there is an error while making the API call
      console.log(error)
      alert('Please enter valid details! and make sure every field is filled!')
    }
  }, [loading, response, error])

  const onSignup = () => {
    console.log('first name: ', firstName)
    console.log('last name: ', lastName)
    console.log('userName: ', userName)
    console.log('contact: ', contact)
    console.log('email: ', email)
    console.log('password: ', password)
    console.log('dob: ', dob)
    dispatch(
      signup(firstName, lastName, userName, contact, email, password, dob)
    )
  }

  return (
    <div>
      <Header title="Signup" />
      <div className="form">
        <div className="mb-3">
          <label className="form-label">First Name</label>
          <input
            onChange={(e) => {
              setFirstName(e.target.value)
            }}
            className="form-control"></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Last Name</label>
          <input
            onChange={(e) => {
              setLastName(e.target.value)
            }}
            className="form-control"></input>
        </div>
        <div className="mb-3">
          <label className="form-label">User Name</label>
          <input
            onChange={(e) => {
              setUserName(e.target.value)
            }}
            className="form-control"></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Contact</label>
          <input
            onChange={(e) => {
              setContact(e.target.value)
            }}
            className="form-control"></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input
            onChange={(e) => {
              setEmail(e.target.value)
            }}
            type="email"
            className="form-control"
            placeholder="test@test.com"
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password</label>
          <input
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            type="password"
            className="form-control"
            placeholder="*****"></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Date Of Birth</label>
          <input
            onChange={(e) => {
              setDob(e.target.value)
            }}
            type="date"
            className="form-control"
          />
        </div>
        <div className="mb-3">
          <button onClick={onSignup} className="btn btn-success">
            Signup
          </button>
          <div className="float-end">
            Existing user? <Link to="/signin">Signin here</Link>
          </div>
        </div>
      </div>

      {userSignup.loading && <div>waiting for result</div>}
    </div>
  )
}

export default SignupScreen
