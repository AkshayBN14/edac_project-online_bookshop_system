import { Link } from 'react-router-dom'
import { Component, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ApiService from '../../service/ApiService'

class DeliveryBoysPendingOrdersScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pendingOrders: [],
      filterBy: null,
      message: null,
    }
    // this.deleteOrderFn = this.deleteOrderFn.bind(this)
    // this.editOrder = this.editOrder.bind(this)
    // this.addOrder = this.addOrder.bind(this)
    this.reloadPendingOrdersList = this.reloadPendingOrdersList.bind(this)
  }

  componentDidMount() {
    this.reloadPendingOrdersList()
  }

  reloadPendingOrdersList() {
    if (this.filterBy == null) {
      ApiService.getAllPendingOrders().then((response) => {
        this.setState({ orders: response.data })
        console.log('All P.Orders: ' + response.data)
        console.log('All P.Orders: ', this.state.orders)
      })
    } else if (this.filterBy == 'CustomerId') {
      //TODO
    } else if (this.filterBy == 'Date') {
      //TODO
    }
  }

  // deleteOrderFn(orderId) {
  //   // e.preventDefault()
  //   // let OrderId = {
  //   //   OrderId: this.state.OrderId,
  //   // }
  //   // const header = {
  //   //   headers: {
  //   //     'Content-Type': 'application/json',
  //   //   },
  //   // }
  //   const body = {
  //     orderId,
  //   }
  //   ApiService.deleteOrder(body).then((res) => {
  //     console.log('RESPONSE:', res)
  //     this.setState({ message: 'Order deleted successfully.' })
  //     this.setState({
  //       orders: this.state.orders.filter((order) => order.id !== orderId),
  //     })
  //   })
  // }

  // changeStatus(order) {
  //   // const body = {
  //   //   order,
  //   // }
  //   console.log('b4 api call: ', order)
  //   ApiService.changeStatus(order).then((res) => {
  //     console.log('RESPONSE:', res)
  //     console.log('b4 api call: ', res.data)
  //     this.setState({ message: 'Order Status changed successfully.' })
  //     // this.setState({
  //     //   orders: this.state.orders.map((order) => order.id == orderId),
  //     // })
  //     // this.reloadOrdersList()
  //   })
  // }
  // onChangeFn = (e) => {
  //   this.setState({ [e.target.name]: e.target.value })
  // }

  render() {
    return (
      <div>
        <h2 className="text-center">All Pending Orders Details</h2>
        <br />
        <br />
        {/* <div>{sessionStorage['msg']}</div> */}
        {/* filter */}
        <div>
          <select
            className="dropdown"
            style={{ width: 300, marginLeft: 400 }}
            id="filter"
            onChange={(e) => {
              this.setState({ filterBy: e.target.value })
              // reloadOrdersList()
            }}>
            <option>FilterBy</option>
            <option value="CustomerId">CustomerId</option>
            <option value="Date">Date</option>
          </select>
          <br />
          <br />
        </div>
        {/* filter */}
        <table
          className="table table-striped"
          style={{ marginLeft: -200, width: 1000 }}>
          <thead>
            <tr>
              {/* <th hidden={this.state.id}>Id</th> */}
              {/* <th className="hidden">Id</th> */}
              <th>OrderId</th>
              <th>OrderDate</th>
              <th>CustomerId</th>
              {/* <th>CustomerName</th> */}
              <th>Books bought</th>
              <th>Total</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {this.state.orders.map((order) => (
              // key={Order.id}>
              <tr>
                <td>{order.id}</td>
                <td>{order.orderDate}</td>
                <td>{order.user.id}</td>
                <td>{order.allBooksQty}</td>
                <td>{order.totalPrice}₹</td>
                <td>{order.status}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => this.deleteOrderFn(order.id)}
                    style={{ width: 70 }}>
                    {' '}
                    Delete
                  </button>
                </td>
                <td>
                  <select
                    className="dropdown"
                    style={{ width: 150 }}
                    id="st"
                    name="status"
                    onChange={(e) => {
                      order.status = e.target.value
                      console.log('onChange of select tag', order.status)
                      console.log('onChange of select tag', order)
                    }}>
                    <option>Change Status</option>
                    <option value="PACKED">PACKED</option>
                    <option value="DISPATCHED">DISPATCHED</option>
                    <option value="DELIVERED">DELIVERED</option>
                  </select>
                </td>
                <td>
                  <button
                    className="btn btn-primary"
                    onClick={(e) => {
                      console.log('aft clicking save button', order)
                      this.changeStatus(order)
                      console.log('after calling changeStatus--->', order)
                    }}
                    style={{ width: 60 }}>
                    {' '}
                    Save
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default DeliveryBoysPendingOrdersScreen
