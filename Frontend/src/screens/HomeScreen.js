import { Component, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Header from '../components/Header'
import { Link } from 'react-router-dom'

import ApiService from '../service/ApiService'

class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: [],
      message: null,
    }
    // this.deleteBookFn = this.deleteBookFn.bind(this)
    // this.editBook = this.editBook.bind(this)
    // this.addBook = this.addBook.bind(this)
    this.reloadBooksList = this.reloadBooksList.bind(this)
  }

  componentDidMount() {
    this.reloadBooksList()
  }

  reloadBooksList() {
    ApiService.getAllBooks().then((response) => {
      this.setState({ books: response.data })
      console.log('All Books: ' + response.data)
      console.log('All Books: ', this.state.books)
    })
  }

  //style="width: 18rem;
  render() {
    return (
      <div>
        <h2 className="text-center">All Books</h2>
        {/* ---- */}
        {/* // cards ------------TODO: CARDS-------------WE */}
        {/* //{' '} */}
        {/* author: '',
        // bookName: '',
        // category: '',
        // copies: 0,
        // description: '',
        // price: 0, */}
        {/* <div class="row">
          {this.state.books.map((book) => (
            <div class="col-md-4">
              <div className="card" style={{ width: 240, height: 200 }}>
                <div className="card-img-top">
                  <div className="card-body">
                    <h5 className="card-title">BookTitle: {book.bookName}</h5>
                    <p className="card-text">Info: {book.description}</p>
                    <p className="card-text">Category: {book.category}</p>
                    <p className="card-text">Price: {book.price}</p>
                    {/* <a href="#" className="btn btn-primary">
              Go somewhere
            </a> */}
        {/* </div>
                </div>
              </div>
            </div>
          ))}
        </div>  */}
        {/* ---- */}
        <table
          className="table table-striped"
          style={{ width: 550, marginLeft: -35 }}>
          <thead>
            <tr>
              {/* <th hidden={this.state.id}>Id</th> */}
              {/* <th className="hidden">Id</th> */}
              <th>Author</th>
              <th>BookName</th>
              <th>Category</th>
              <th>Copies</th>
              <th>Description</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {this.state.books.map((book) => (
              // key={book.id}>
              <tr>
                <td>{book.author}</td>
                <td>{book.bookName}</td>
                <td>{book.category}</td>
                <td>{book.copies}</td>
                <td>{book.description}</td>
                <td>{book.price}</td>
                {/* <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => this.deleteBookFn(book.id)}
                    style={{ width: 70 }}>
                    Delete
                  </button>
                </td>
                <td>
                  <button
                    className="btn btn-success"
                    onClick={() => this.editBook(book.id)}
                    style={{ marginLeft: '20px' }}
                    style={{ width: 70 }}>
                    {' '}
                    Edit
                  </button>
                </td> */}
              </tr>
            ))}
          </tbody>
        </table>
        {/* <div className="row"> */}
        Existing User
        <br />
        <div className="btn btn-success">
          <Link to="/signin" style={{ color: 'white' }}>
            Signin here
          </Link>
        </div>
        <div className="float-end btn btn-success">
          <Link to="/signup-customer" style={{ color: 'white' }}>
            Signup here
          </Link>
        </div>
        <div className="float-end">New User?</div>
        {/* </div> */}
      </div>
    )
  }
}

export default HomeScreen
