import axios from 'axios'

const USER_API_BASE_URL = 'http://localhost:8080'

//1) ADMIN

//2) CUSTOMER
//- /user/register-as-customer

//3) DELIVERY BOY
//- /user/register-as-delivery-boy

class ApiService {
  // header = {
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  // }

  signUp(body) {
    console.log('Role of current signed up user:', body.role)
    if (body.role == 'CUSTOMER')
      return axios.post(USER_API_BASE_URL + '/user/register-as-customer', body)
    else if (body.role == 'DELIVERY_BOY')
      return axios.post(
        USER_API_BASE_URL + '/user/register-as-delivery-boy',
        body
      )
  }

  //signIn for ALL
  signInForAll(body) {
    console.log('BODY: ', body)
    return axios.post(USER_API_BASE_URL + '/user/login', body)
  }

  getAllBooks() {
    return axios.get(USER_API_BASE_URL + '/admin/books')
  }
  getAllCategories() {
    return axios.get(USER_API_BASE_URL + '/book/get-all-categories')
  }
  getAllAuthors() {
    return axios.get(USER_API_BASE_URL + '/book/get-all-authors')
  }

  addBook(book) {
    return axios.post(USER_API_BASE_URL + '/book/add-book', book) //admin
  }
  deleteBook(bookId) {
    return axios.post(
      USER_API_BASE_URL + '/book/delete-book-by-id',
      bookId /*{ body.bookId }*/ //TODO !!
    ) //admin
  }

  fetchBookById(bookId) {
    return axios.post(USER_API_BASE_URL + '/book/book-by-id' + '/' + bookId)
  }

  editBook(book) {
    console.log('BOOOOOOK!', book)
    console.log('in edit book ApiService')
    return axios.put(
      USER_API_BASE_URL + '/book/update-book' + '/' + book.id,
      book
    )
  }
  getAllOrders() {
    return axios.get(USER_API_BASE_URL + '/admin/all-orders')
  }
  changeStatus(order) {
    console.log('in ApiService changeStatus')
    console.log(order)
    console.log(order.status)
    return axios.put(USER_API_BASE_URL + '/admin/change-order-status', order)
  }

  fetchAllCustomers() {
    return axios.get(USER_API_BASE_URL + '/admin/customers-list')
  }
  fetchAllDeliveryBoys() {
    return axios.get(USER_API_BASE_URL + '/admin/deliveryboys-list')
  }
  deleteUser(userId) {
    return axios.delete(USER_API_BASE_URL + '/admin/delete-user/' + userId)
  }
  fetchUserById(userId) {
    return axios.get(USER_API_BASE_URL + '/admin/get-user/' + userId)
  }
  editUser(user) {
    return axios.put(
      USER_API_BASE_URL + '/user/update-profile/' + user.id,
      user
    )
  }

  //-------------------------
  //from ma'am code
  addUser(user) {
    return axios.post('' + USER_API_BASE_URL, user)
  }
}

export default new ApiService()
