import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { logout } from '../actions/userActions'

const Navigation = (props) => {
  // const cart = useSelector((store) => store.cart)
  const dispatch = useDispatch()
  const userSignin = useSelector((store) => store.userSignin)
  const { loading, response, error } = userSignin

  // for cart items
  // const cartItemsStore = useSelector((state) => state.cartItemsStore)

  const onLogout = () => {
    dispatch(logout())
  }

  // let someoneLoggedIn = false
  // //getting the token id from the sesionStorage and check if any user is logged in or not
  // if (sessionStorage.getItem('tokenId') != undefined) someoneLoggedIn = true

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link to="/home" style={{ textDecoration: 'none' }}>
            <span className="navbar-brand">BookShop</span>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link to="/home" style={{ textDecoration: 'none' }}>
                  <span className="nav-link">Home</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/about" style={{ textDecoration: 'none' }}>
                  <span className="nav-link">About</span>
                </Link>
              </li>
              {/* {(userSigninStore.response &&
                userSigninStore.response.status &&
                userSigninStore.response.status == 'error') ||
                (userSigninStore.response == null && (
                  <li className="nav-item">
                    <Link to="/signin">
                      <span className="nav-link">SignIn</span>
                    </Link>
                  </li>
                ))} */}
              {/* need to chek the ersponse.status value to be 'success' or 'error'
              {(userSigninStore.response &&
                userSigninStore.response.status &&
                userSigninStore.response.status == 'error') ||
                (userSigninStore.response == null && (
                  <li className="nav-item">
                    <Link to="/signup-as-customer">
                      <span className="nav-link">SignUp as Customer</span>
                    </Link>
                  </li>
                ))} */}
              {/* {(userSigninStore.response &&
                userSigninStore.response.status &&
                userSigninStore.response.status == 'error') ||
                (userSigninStore.response == null && (
                  <li className="nav-item">
                    <Link to="/signup-as-deliveryboy">
                      <span className="nav-link">SignUp as Delivery Boy</span>
                    </Link>
                  </li>
                ))} */}
              {/* {userSigninStore.response &&
                userSigninStore.response.status &&
                userSigninStore.response.status == 'success' && (
                  <li className="nav-item">
                    <Link to="/seller">
                      <span className="nav-link">Seller</span>
                    </Link>
                  </li>
                )} */}
              {/* {userSigninStore.response &&
                userSigninStore.response.status &&
                userSigninStore.response.status == 'success' && (
                  <li className="nav-item">
                    <Link to="/admin">
                      <span className="nav-link">Admin</span>
                    </Link>
                  </li>
                )} */}
              {/* {userSigninStore.response &&
                userSigninStore.response.status &&
                userSigninStore.response.status == 'success' && (
                  <li className="nav-item">
                    <Link to="/edit-profile">
                      <span className="nav-link">Edit Profile</span>
                    </Link>
                  </li>
                )} */}
            </ul>
            {/* <div className="d-flex" /> */}
            {/* // for the cart functionality !!!!!!!!!!!!!!!! */}
            {/* {cartItemsStore &&
              cartItemsStore.response &&
              cartItemsStore.response.data && (
                <span>
                  <ul class="navbar-nav flex-row justify-content-end flex-wrap align-items-center mr-lg-4 mr-xl-0">
                    <li class="nav-item px-3 text-uppercase mb-0 position-relative d-lg-flex">
                      <div id="cart" class="d-none" />
                      <Link
                        to="/cart"
                        class="cart position-relative d-inline-flex"
                        aria-label="View your shopping cart">
                        <i class="fas fa fa-shopping-cart fa-lg" />
                        <span class="cart-basket d-flex align-items-center justify-content-center">
                          {cartItemsStore.response.data.length}
                        </span>
                      </Link>
                    </li>
                  </ul>
                </span>
              )} */}
            {/* {userSigninStore.response &&
              userSigninStore.response.status &&
              userSigninStore.response.status == 'success'} */}
            {userSignin.response && (
              <div className="d-flex">
                <button onClick={onLogout} className="btn btn-outline-success">
                  Logout
                </button>
              </div>
            )}
          </div>
        </div>
      </nav>
    </div>
  )
}

export default Navigation
