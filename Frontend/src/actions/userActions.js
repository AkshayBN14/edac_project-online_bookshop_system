import {
  USER_SIGNIN_FAIL,
  USER_SIGNIN_REQUEST,
  USER_SIGNIN_SUCCESS,
  USER_SIGNOUT,
  USER_SIGNUP_FAIL,
  USER_SIGNUP_REQUEST,
  USER_SIGNUP_SUCCESS,
} from '../constants/userConstants'
import axios from 'axios'
import ApiService from '../service/ApiService'

export const logout = () => {
  return (dispatch) => {
    sessionStorage.removeItem('tokenId')
    dispatch({ type: USER_SIGNOUT })
    document.location.href = '/signin'
  }
}

export const signup = (
  firstName,
  lastName,
  userName,
  contact,
  email,
  password,
  dob
) => {
  return (dispatch) => {
    dispatch({
      type: USER_SIGNUP_REQUEST,
    })

    const header = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const body = {
      firstName,
      lastName,
      userName,
      contact,
      email,
      password,
      dob,
    }
    // const url = 'http://localhost:4000/user/'
    //'/user/register-as-customer', body, header
    //post(url, body, header)
    ApiService.signUpAsCustomer(body, header)
      .then((response) => {
        dispatch({
          type: USER_SIGNUP_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type: USER_SIGNUP_FAIL,
          payload: error,
        })
      })
  }
}
//--------------------------------------------------------------------------
export const signin = (email, password) => {
  return (dispatch) => {
    dispatch({
      type: USER_SIGNIN_REQUEST,
    })

    const header = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const body = {
      email,
      password,
    }
    // const url = 'http://localhost:8080/user/login'
    ApiService.signInForAll(body, header)
      .then((response) => {
        dispatch({
          type: USER_SIGNIN_SUCCESS,
          payload: response.data,
        })
      })
      .catch((error) => {
        dispatch({
          type: USER_SIGNIN_FAIL,
          payload: error,
        })
      })
  }
}
