package com.app.pojos;

public enum Role {
	ADMIN, CUSTOMER, DELIVERY_BOY
}
