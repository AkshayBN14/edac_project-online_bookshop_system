package com.app.pojos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*BookName	varchar(40)
Category(ENUM)	ENUM-Category
Description	varchar(100)
Author	varchar(30)
Price 	double
Copies	int*/

//ISBN !!
//PublishedDate !!

@Entity
@Table(name = "books")
public class Book extends BaseEntity {
	
	@Column(name = "book_name",length = 20,nullable = false, unique = true)
	private String bookName;
	
	@Column(name = "author",length = 20,nullable = false)
	private String author;
	
	@Column(name = "description",length = 200, nullable = false)
	private String description;
	
	@Min(0)
	@Column(nullable = false)
	private double price;
	
//	@JsonIgnore
	@Min(0)
	@Column(nullable = false)
	private int copies;
	
	//ISBN !!!
	
	@Column(nullable = false, length = 20)
	@Enumerated(EnumType.STRING)
	private Category category;
	
	//add a column for storing the path of the book fileName in varchar 
	@Column(name = "book_img_file_name", length = 200, unique = true)
	private String bookImgFileName;
	
//	@JsonIgnore
//	@ManyToMany(cascade = {CascadeType.MERGE,CascadeType.PERSIST},mappedBy = "userBooks", fetch = FetchType.LAZY)
//	@JsonIgnoreProperties("userBooks") // for preventing stackoverflow
//	@Fetch(FetchMode.JOIN)
//	private List<User> users = new ArrayList<>(); //read abt set collection
	
	@JsonIgnore //--NEW-- look in OrderDetails
	@OneToMany (mappedBy = "book", cascade = CascadeType.ALL)
	@JsonIgnoreProperties("book")
	private List<OrderDetails> ordersDetails = new ArrayList<>();
	
	// OrderDetails 1 -> 1 Book ----Unidirectional
	public Book() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public Book(String bookName, String author, String description, double price, int copies,
			Category category/*, Set<User> users*/) {
		super();
		this.bookName = bookName;
		this.author = author;
		this.description = description;
		this.price = price;
		this.copies = copies;
		this.category = category;
		/* this.users = users; */
	}
	

	public Book(String bookName, String author, String description, @Min(0) double price, @Min(0) int copies,
			Category category, String bookImgFileName) {
		super();
		this.bookName = bookName;
		this.author = author;
		this.description = description;
		this.price = price;
		this.copies = copies;
		this.category = category;
		this.bookImgFileName = bookImgFileName;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCopies() {
		return copies;
	}

	public void setCopies(int copies) {
		this.copies = copies;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getBookImgFileName() {
		return bookImgFileName;
	}

	public void setBookImgFileName(String bookImgFileName) {
		this.bookImgFileName = bookImgFileName;
	}

	public List<OrderDetails> getOrdersDetails() {
		return ordersDetails;
	}

	public void setOrdersDetails(List<OrderDetails> ordersDetails) {
		this.ordersDetails = ordersDetails;
	}

	@Override
	public String toString() {
		return "Book [BookId: "+ getId() +" bookName=" + bookName + ", author=" + author + ", description=" + description + ", price=" + price
				+ ", copies=" + copies + ", category=" + category + ", bookImgFileName=" + bookImgFileName + "]";
	}

}


//BCZ WE DONT NEED CART_ID IN BOOK TABLE so we KEPT it as UNIDIRECTIONAL!!!!!!!!!!!
//@ManyToOne(fetch = FetchType.LAZY) //--NEW-- look in Book
//@JoinColumn(name = "cart_id")
//private Cart cart;

















