package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "address")
public class Address extends BaseEntity {
	
	@Column(length = 10)
//	@NotBlank(message = "Flat No must be entered")
	private String flatNo;
	@Column(length = 20)
//	@NotBlank(message = "Society must be entered")
	private String society;
	@Column(length = 20)
//	@NotBlank(message = "City must be entered")
	private String city;
	@Column(length = 30)
//	@NotBlank(message = "State must be entered")
	private String state;
	@Column(length = 30)
//	@NotBlank(message = "Country must be entered")
	private String country;
	@Column(length = 20)
//	@NotBlank(message = "Pincode must be entered")
	private int pincode;
	//ONE TO ONE UNIDIRECTIONAL ASSOCIATION Between entities 
	//(User 1 <--- 1 Address)
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	public Address() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public Address(String flatNo, String society, String city, String state, String country,
			int pincode /* , User user */) {
		super();
		this.flatNo = flatNo;
		this.society = society;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
//		this.user = user;
	}

	public String getFlatNo() {
		return flatNo;
	}

	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society = society;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Address [AddressId/UserId: "+ getUser().getId() +" flatNo=" + flatNo + ", society=" + society + ", city=" + city + ", state=" + state
				+ ", country=" + country + ", pincode=" + pincode + "]";
	}
	
	
}

//@MapsId //to tell hibernate : use shared PK approach (single col will act as PK(for user table) & FK(for addr table) referring to User id)







