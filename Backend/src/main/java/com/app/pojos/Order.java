package com.app.pojos;

//import java.sql.Date;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//OrderID(PK)	int
//UserID(FK)	int -- user
//OrderDate	DATE
//OrderStatus	ENUM-STATUS
//BooksOrdered	int
//PaidAmount	double
//TotalAmount	double
//User<->Order	OneToMany


@Entity
@Table(name = "orders")
public class Order extends BaseEntity{
	
	//initialize this field order date in the ctor itself
	@Column(name = "order_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd") 
//	@Temporal(TemporalType.DATE) // added NEWLY!!! --- dd-mm-yyyyy -- SDF
	//@PastOrPresent(message = "")
	private LocalDate orderDate = LocalDate.now(); // as the order gets sent in the response as a state...if we initialize it in constructor then the date doesn't get sent in response  
	//initialized in Constructor itself
	
	@Column(length = 12,nullable = false)
	@Enumerated(EnumType.STRING)
	private OrderStatus status;
	
	@Min(0)
	@Column(name = "all_books_qty",nullable = false)
	private int allBooksQty;
	
	@Min(0)
	@Column(name = "total_price",nullable = false)
	private double totalPrice; //added newly
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id") //FK
	@JsonIgnoreProperties("orders")
	private User user = new User();
	
	@JsonIgnore
	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<OrderDetails> orderDetails = new ArrayList<>();
	

	public Order() {
		 System.out.println("in POJO: "+getClass().getName());
	}
	
	public Order(OrderStatus status,int allBooksQty/* , User user, List<OrderDetails> orderDetails */) {
		super();
//		this.orderDate = LocalDate.now();
		this.status = status;
		this.allBooksQty = allBooksQty;
//		this.user = user;
//		this.orderDetails = orderDetails;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}
	public int getAllBooksQty() {
		return allBooksQty;
	}
	public void setAllBooksQty(int allBooksQty) {
		this.allBooksQty = allBooksQty;
	}
	
	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<OrderDetails> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}
	@Override
	public String toString() {
		return "Order [Id: "+ getId() +", orderDate=" + orderDate + ", status=" + status + ", allBooksQty=" + allBooksQty + "]";
	}
	
	
}












