package com.app.pojos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "users")
public class User extends BaseEntity {

	@Column(name = "first_name", length = 30, nullable = false)
//	@NotBlank(message = "First Name must be entered")
	private String firstName;

	@Column(name = "last_name", length = 30, nullable = false)
//	@NotBlank(message = "Last Name must be entered")
	private String lastName;

	// enter email OR userName to login
	@Column(name = "user_name", length = 30, unique = true, nullable = false)
//	@NotBlank(message = "User Name must be entered")
	private String userName;

	@Column(length = 15, unique = true, nullable = false)
//	@NotBlank(message = "Contact must be entered")
	private String contact;

	@Column(unique = true, nullable = false, length = 25)
//	@Email(message = "Invalid email format")
//	@Length(min = 5,max=30,message = "Invalid Email length")
//	@NotBlank(message = "Email is required")
	private String email;

	@Column(length = 25, nullable = false)
//	@Pattern(regexp="((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})",message = "Invalid password")
	private String password;

	@Column(length = 30, nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;

	@Column(name = "date_of_birth")
	@DateTimeFormat(pattern = "yyyy-MM-dd") // SC knows to take input of data in US format mm-dd-yyyyy
	@Temporal(TemporalType.DATE) // added NEWLY!!! --- dd-mm-yyyyy
//	@PastOrPresent(message = "Invalid date of birth")
	private Date dob;

	//commented later to avoid the unnecessary table of user_book_combine (Book-User realtion)
//	@JsonIgnore
//	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
//	@JoinTable(name = "user_book_combine", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "book_id"))
////	@JsonIgnoreProperties("users")
//	private List<Book> userBooks = new ArrayList<>(); // added NEWLY!!!

	// orphanRemoval = true ----> TODO !!
	@JsonIgnore
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
//	@JsonIgnoreProperties("user")
	private List<Order> orders = new ArrayList<>();

	// to keep Address-->Students Unidirectional
//	@OneToOne //TODO
//	private Address address;

//	@OneToOne // bcz we need to keep cart->user as unidirectional
//	private Cart cart;

	public User() {
		System.out.println("in POJO: " + getClass().getName());
	}

	public User(String firstName, String lastName, String userName, String contact, String email, String password,
			Role role, Date dob) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.contact = contact;
		this.email = email;
		this.password = password;
		this.role = role;
		this.dob = dob;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}


	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", userName=" + userName + ", contact="
				+ contact + ", email=" + email + ", role=" + role + ", dob=" + dob + "]";
	}

}

//@JsonIgnoreProperties("users")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//Hibernate.initialize(result.getIngredients());

//@OneToOne
//@Embedded //totally optional
//private Cart cart; // LOOK FOR VALUE TYPE "CART"
//how to tell Hibr about embeddable type
// Entity to Value Association ---- NEW
// one to one uni dir association betwn entity(Student)-->Value(AadharCard)

//how to tell Hibr about embeddable type
// Entity to Value Association ---- NEW
// one to one uni dir association betwn entity(User)-->Value(Address)
//	@Embedded // totally optional
//	private Address address;