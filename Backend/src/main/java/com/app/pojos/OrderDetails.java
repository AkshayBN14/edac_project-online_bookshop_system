package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

//OrderDetailsID(PK)	int
//OrderID(FK)	int
//BookID	int
//Quantity	int
//OrderDetails <-> Book ----OneToOne

@Entity
@Table(name = "order_details")
public class OrderDetails extends BaseEntity{
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id") //FK
	private Order order;
	
	//nullable = false ---> TODO TOCHECK -- changed from OneToOne to ManyToOne
//	@OneToOne
//	@JoinColumn(name = "book_id" /*, nullable = false*/)
//	private Book book;
	
	@ManyToOne(fetch = FetchType.LAZY) //--NEW-- look in Book
	@JoinColumn(name = "book_id")
	private Book book;
	
	//QUANTITY PER BOOK
	@Min(0)
	@Column(name = "qty_per_book", nullable = false)
	private int qtyPerBook; // per book qty

	public OrderDetails() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public OrderDetails(Order order, Book book, int qtyPerBook) {
		super();
		this.order = order;
		this.book = book;
		this.qtyPerBook = qtyPerBook;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public int getQtyPerBook() {
		return qtyPerBook;
	}

	public void setQtyPerBook(int qtyPerBook) {
		this.qtyPerBook = qtyPerBook;
	}

	@Override
	public String toString() {
		return "OrderDetails [orderId=" + order.getId() + ", bookName =" + book.getBookName()
		+ ", qtyPerBook=" + qtyPerBook + "]";
	}
	
	
	
}
