package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.OrderStatus;
import com.app.service.IBookService;
import com.app.service.ICustomerService;
import com.app.service.IOrderService;

@RestController
@RequestMapping("/delivery-boy")
@CrossOrigin(origins = "http://localhost:3000")
public class DeliveryBoyController {

	//dependency
		@Autowired
		private ICustomerService custService;
		@Autowired
		private IBookService bookService;
		@Autowired
		private IOrderService orderService;
		
		public DeliveryBoyController() {
			System.out.println("in ctor of : "+getClass().getName());
		}

		@GetMapping(value = "/home")
		public ResponseEntity<?> deliveryHome(){
			System.out.println("in delivery-boy home page!"); // create react component for admin home page
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		@GetMapping(value = "/pending-orders") //the orders whose status is PACKED
		public ResponseEntity<?> pendingOrders(){
			System.out.println("in delivery-boy pending orders!"); 
			return new ResponseEntity<>(orderService.getAllPendingOrders(OrderStatus.PACKED), HttpStatus.OK);
		}
		
	
}
