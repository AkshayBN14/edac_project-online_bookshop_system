package com.app.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.UserDTO;
import com.app.service.IBookService;
import com.app.service.ICustomerService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	//dependency
	@Autowired
	private ICustomerService custService;
	@Autowired
	private IBookService bookService;
	
	public AdminController() {
		System.out.println("in ctor of : "+getClass().getName());
	}
	
	@GetMapping(value = "/customers-list")
	public ResponseEntity<?> listAllCustomers(){
		return new ResponseEntity<>(custService.getAllCustomers(), HttpStatus.OK);
	}
	
	//GETcUSTdETAILS BY EMAIL
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public ResponseEntity<?> getCustomersDetails(@RequestBody Map<String, Object> req){
		String email = (String) req.get("email");
		System.out.println(email);
//		System.out.println(custService.getCustomersDetails(email));
		return new ResponseEntity<>(custService.getCustomersDetails(email), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/delete-user", method = RequestMethod.POST)
	public ResponseEntity<?> deleteCustomersDetails(@RequestBody Map<String, Object> req){
		String email = (String) req.get("email");
		System.out.println(email);
		return new ResponseEntity<>(custService.deleteCustomersDetails(custService.getCustomersDetails(email)), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/add-customer", method = RequestMethod.POST)
	public ResponseEntity<?> addCustomer(/* @Valid */@RequestBody UserDTO userDTO){ //admin
		System.out.println("in adminCtlr : "+userDTO);
		return new ResponseEntity<>(custService.addCustomer(userDTO),HttpStatus.OK); 
	}
	
}
