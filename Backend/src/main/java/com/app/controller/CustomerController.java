package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.CartDTO;
import com.app.service.IOrderService;
import com.app.service.IUserService;

@RestController
@RequestMapping("/customer")
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
	// dependency
	@Autowired
	private IUserService custService;
//	@Autowired
//	private ICartService cartService;
	@Autowired
	private IOrderService orderService;
//	@Autowired
//	private ServletContext servletCtxt; //for accessing application scope -- NOT TO BE USED!!

	
	@GetMapping(value = "/home")
	public ResponseEntity<?> customerHome(){
		System.out.println("in customer home page!"); // create react component for admin home page
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	//to DISPLAY CART we don't need a backend call just display cart in frontend by accessing it via localStorage(frontend)
	
	//place order :TODOOOOOOOO
	@RequestMapping(value = "/place-order/{userId}", method = RequestMethod.POST)
	public ResponseEntity<?> placeOrder(@PathVariable("userId") int userId, @RequestBody CartDTO cartDTO){ // we can use @RequestHeaer to get the userd as a TOKEN
		
		cartDTO.setUserId(userId);
		
		System.out.println("in placeOrder controller method!");
		System.out.println(userId);
		System.out.println(cartDTO);
		
		return new ResponseEntity<>(orderService.placeOrder(userId, cartDTO), HttpStatus.OK);
	}
	


// viewALLOrders
	@GetMapping(value = "/view-all-orders") //the orders whose status is PACKED
	public ResponseEntity<?> getCustomerOrders(@RequestHeader int userId){
		System.out.println("in cust view orders method!"); 
		return new ResponseEntity<>(orderService.getCustomerOrdersById(userId), HttpStatus.OK);
	}




	// to increment the quantity of book
	
	// get the cart for current user -> add all cart books to orders -> 
//	1.place order->need to fetch current user cart ->setstatus to packed =>set all books size as per qty ordered 
//	2 Set user ->either object id /whole user object
//	3.initialize order details 
//	a.ordder,book , oder qty
	
	
	
	// viewALLOrders

}
////placeOrder
//	@RequestMapping(value = "/place-order/{userId}", method = RequestMethod.POST)
//	public ResponseEntity<?> placeOrder(@PathVariable int userId) {
//		if (servletCtxt.getAttribute("allCartsFromApplScope") == null) { // in the start of appln-no cart present in appln scope
//			return new ResponseEntity<>("Cart not created! You need to add a book to cart to place the order!", HttpStatus.OK);
//		}else { // some carts are present
//			List<Cart> allCarts = (List<Cart>) servletCtxt.getAttribute("allCartsFromApplScope");
//			Cart currentUserCart = null;
//			System.out.println(allCarts);
//			boolean flag = false;
//			for (Cart c : allCarts) {
//				if (c.getUser().getId() == userId) {// true if OLD USER i.e. his/her cart is present in appln scope
//					flag=true; // user cart found
//					currentUserCart = c;
//					break;
//				} 
//			}
//			if (flag==false) {// NEW USER i.e. his/her cart is not added in the appln scope
//				return new ResponseEntity<>("Cart not created! You need to add a book!", HttpStatus.OK);
//			}
//			return new ResponseEntity<>(orderService.placeOrder(currentUserCart), HttpStatus.OK);
//		}
//	}

////add to cart --- in react: `${userId}`
//	@RequestMapping(value = "/add-to-cart/{userId}", method = RequestMethod.POST)
//	public ResponseEntity<?> addBookToCart(@PathVariable int userId, @RequestBody Map<String, Integer> req) {
//		int bookId = req.get("bookId"); // bookId
//		System.out.println("bookId: " + bookId);
//		
//		//session scope 
////		System.out.println("req: " + req);
//		// add cart of all users in ServletContext (Appln Scope)
//		List<Cart> allCarts = new ArrayList<>(); // allCarts here is local variable/ref
//		Cart currentUserCart = cartService.getCartByUserId(userId);
//		if (currentUserCart==null)
//			currentUserCart = cartService.assignCartToUser(userId); //assigned new cart to user
//		System.out.println("---");
//		if (servletCtxt.getAttribute("allCartsFromApplScope") == null) {// in the start of appln - no cart present in appln scope
//			System.out.println("===");
//			allCarts.add(cartService.getCartByUserId(userId)); //adding current user cart into list
//			System.out.println("///");
//			servletCtxt.setAttribute("allCartsFromApplScope", allCarts); //setting list in appln scope as a <K,V> pair
//			for (Cart c : allCarts) { //iterating thru allCarts
//				if (c.getUser().getId() == userId) {
//					System.out.println("%%%");
//					currentUserCart = c;
//					System.out.println("+++");
//					break;
//				}
//			}
//		} else { // control comes here that means carts are already PRESENT in appln scope
//			//below allCarts is the one from appln scope
//			allCarts = (List<Cart>) servletCtxt.getAttribute("allCartsFromApplScope");
//			System.out.println(allCarts);
//			System.out.println("***");
//			boolean flag = false;
//			for (Cart c : allCarts) {
//				if (c.getUser().getId() == userId) {// true if OLD USER
//					flag=true; // user cart found
//					currentUserCart = c;
//					System.out.println("&&&");
//					break;
//				} 
//			}
//			if (flag==false) {// NEW USER i.e. his/her cart is not added in the appln scope
//				System.out.println("(((");
//				currentUserCart = cartService.getCartByUserId(userId);
//				System.out.println(currentUserCart);
//				System.out.println(allCarts);
//				allCarts.add(currentUserCart); // cart added in appln scope for NEW USER
//			}
//		}
//		return new ResponseEntity<>(cartService.addBookToCart(currentUserCart, bookId), HttpStatus.OK);
//	}
//
//
//


////displayCart
//	@RequestMapping(value = "/display-cart/{userId}", method = RequestMethod.GET)
//	public ResponseEntity<?> displayCart(@PathVariable int userId) {
//		if (servletCtxt.getAttribute("allCartsFromApplScope") == null) { // in the start of appln-no cart present in appln scope
//			return new ResponseEntity<>("Cart not created! You need to add a book!", HttpStatus.OK);
//		}else { // some carts are present
//			List<Cart> allCarts = (List<Cart>) servletCtxt.getAttribute("allCartsFromApplScope");
//			Cart currentUserCart = null;
//			System.out.println(allCarts);
//			boolean flag = false;
//			for (Cart c : allCarts) {
//				if (c.getUser().getId() == userId) {// true if OLD USER i.e. his/her cart is present in appln scope
//					flag=true; // user cart found
//					currentUserCart = c;
//					break;
//				} 
//			}
//			if (flag==false) {// NEW USER i.e. his/her cart is not added in the appln scope
//				return new ResponseEntity<>("Cart not created! You need to add a book!", HttpStatus.OK);
//			}
//			return new ResponseEntity<>(currentUserCart, HttpStatus.OK);
//		}
//	}