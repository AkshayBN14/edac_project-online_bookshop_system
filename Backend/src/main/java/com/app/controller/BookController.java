package com.app.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;

import com.app.dto.BookDTO;
import com.app.pojos.Book;
import com.app.pojos.Category;
import com.app.service.IBookService;

@RestController
@RequestMapping("/book")
public class BookController {
	//dependency
	@Autowired
	private IBookService bookService;

	public BookController() {
		System.out.println("in ctor of: "+getClass().getName());
	}
	
	@RequestMapping(value = "/list-all-books", method = RequestMethod.GET)
	public ResponseEntity<?> getAllBooksDetails(){
		return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/books-by-category", method = RequestMethod.POST)
	public ResponseEntity<?> getBooksByCategory(@RequestBody Map<String, Category> req){
		Category category = req.get("category");
		System.out.println(category);
		System.out.println(bookService.getBooksByCategory(category));
		return new ResponseEntity<>(bookService.getBooksByCategory(category), HttpStatus.OK);
	}
	@RequestMapping(value = "/books-by-name", method = RequestMethod.POST)
	public ResponseEntity<?> getBooksByName(@RequestBody Map<String, String> req){
		String bName = req.get("bookName");
		System.out.println(bName);
		System.out.println(bookService.getBooksByName(bName));
		return new ResponseEntity<>(bookService.getBooksByName(bName), HttpStatus.OK);
	}
	@RequestMapping(value = "/books-by-price", method = RequestMethod.POST)
	public ResponseEntity<?> getBooksByPrice(@RequestBody Map<String, Double> req){
		double bPrice = req.get("price");
		System.out.println(bPrice);
		System.out.println(bookService.getBooksByPriceLessThan(bPrice));
		return new ResponseEntity<>(bookService.getBooksByPriceLessThan(bPrice), HttpStatus.OK);
	}
	@RequestMapping(value = "/books-by-author", method = RequestMethod.POST)
	public ResponseEntity<?> getBooksByAuthor(@RequestBody Map<String, String> req){
		String bAuthor = req.get("author");
		System.out.println(bAuthor);
		System.out.println(bookService.getBooksByAuthor(bAuthor));
		return new ResponseEntity<>(bookService.getBooksByAuthor(bAuthor), HttpStatus.OK);
	}
	//--------------------------
	@RequestMapping(value = "/add-book", method = RequestMethod.POST)
	public ResponseEntity<?> addBook(@RequestBody BookDTO bookDTO){ //admin
		System.out.println("in bookCtlr : "+bookDTO);
		return new ResponseEntity<>(bookService.addBook(bookDTO),HttpStatus.CREATED); //created!
	}
	@RequestMapping(value = "/delete-book-by-name", method = RequestMethod.POST)
	public ResponseEntity<?> addBook(@RequestBody Map<String, String> req){ //admin
		String bName = req.get("bookName");
		System.out.println("in bookCtlr : "+req);
		return new ResponseEntity<>(bookService.deleteBookByName(bName),HttpStatus.OK); 
	}
	//--------------------------
	@RequestMapping(value = "/increment-copies", method = RequestMethod.PUT) //admin
	public ResponseEntity<?> incrementBookCopies(@RequestBody Map<String, Integer> req){ //admin
		int bookId = req.get("id");
		// LOOK FOR Strategy for passing this "nos" from frontend!!!!
		int nos = req.get("nos"); 
		System.out.println("in bookCtlr : "+bookId);
		return new ResponseEntity<>(bookService.incrementBookCopies(bookId, nos),HttpStatus.OK);
	}
	@RequestMapping(value = "/decrement-copies", method = RequestMethod.PUT) //admin
	public ResponseEntity<?> decrementBookCopies(@RequestBody Map<String, Integer> req){ //admin
		int bookId = req.get("id");
		// LOOK FOR Strategy for passing this "nos" from frontend!!!!
		int nos = req.get("nos"); 
		System.out.println("in bookCtlr : "+bookId);
		return new ResponseEntity<>(bookService.decrementBookCopies(bookId, nos),HttpStatus.OK);
	}
	//-----------------------
	//edit/update book (except id and bookName)
	@RequestMapping(value = "/update-book/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateBook(@PathVariable Integer id, @RequestBody BookDTO bookDTO){ //admin
		System.out.println("in bookCtlr -id: "+id);
		System.out.println(bookDTO);
		return new ResponseEntity<>(bookService.updateBook(id, bookDTO),HttpStatus.OK); 
	}
	//-----------------------
}












