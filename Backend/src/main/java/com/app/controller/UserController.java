package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.AddressDTO;
import com.app.dto.LoginRequest;
import com.app.dto.UserDTO;
import com.app.pojos.User;
import com.app.service.IUserService;

//Login 
//Register

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

	@Autowired
	private IUserService userService;

	// REGISTER AS ADMIN=> signup
	@RequestMapping(value = "/register-as-admin", method = RequestMethod.POST)
	public ResponseEntity<?> registerAsAdmin(@RequestBody UserDTO userDTO) {
		System.out.println("in userCtlr : " + userDTO);
		return new ResponseEntity<>(userService.registerAsAdmin(userDTO), HttpStatus.OK);
	}

	// REGISTER AS CUSTOMER => signup
	@RequestMapping(value = "/register-as-customer", method = RequestMethod.POST)
	public ResponseEntity<?> registerAsUser(@RequestBody UserDTO userDTO) { 
		System.out.println("in userCtlr : " + userDTO);
		return new ResponseEntity<>(userService.registerAsCustomer(userDTO), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/register-as-delivery-boy", method = RequestMethod.POST)
	public ResponseEntity<?> registerAsDeliveryBoy(@RequestBody UserDTO userDTO) { 
		System.out.println("in userCtlr : " + userDTO);
		return new ResponseEntity<>(userService.registerAsDeliveryBoy(userDTO), HttpStatus.OK);
	}

	@PostMapping("/login") // =>signin for Both ADMIN AND CUSTOMER
	public ResponseEntity<?> authenticateCustomer(@RequestBody LoginRequest request, HttpSession session) { //sesion is used here to store current user logged in details in session scope
		System.out.println("in auth " + request);
		User user = userService.authenticateUser(request.getEmail(), request.getPassword());
		
		//storing whole object of current user in session scope to access it thorughour the session - from login to logout
		session.setAttribute("currentUserDetails", user);
		
		System.out.println("User : " + user);
//		if (user!=null)
//			map.put("msg", "Authenticated Successfull!");
		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
		return new ResponseEntity<>("Authenticated Successfull!", HttpStatus.OK);

	}

	// edit/update user (except email and userName)
	@RequestMapping(value = "/update-profile/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUser(@PathVariable Integer id, @RequestBody UserDTO userDTO) { // for All
		System.out.println("in userCtlr -id: " + id);
		System.out.println(userDTO);
		return new ResponseEntity<>(userService.updateProfile(id, userDTO), HttpStatus.OK);
	}
	@RequestMapping(value = "/add-address/{userId}", method = RequestMethod.POST)
	public ResponseEntity<?> addAddress(@PathVariable Integer userId, @RequestBody AddressDTO addressDTO){
		System.out.println("in userCtlr : "+addressDTO);
		return new ResponseEntity<>(userService.addAddress(userId, addressDTO),HttpStatus.OK);
	}
	@GetMapping("/logout")
	public ResponseEntity<?> userLogout(HttpSession session, HttpServletRequest req, HttpServletResponse resp) {
		try {
			System.out.println("in user logout ....");
			// add user details as a model attribute , read from session scope
			String userDetails = (String) session.getAttribute("currentUserDetails");
			System.out.println("Logged out User: "+userDetails);
			// invalidate session
			session.invalidate();
//			resp.setHeader("refresh", "5;url="+"/login");
			resp.sendRedirect("/login");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>("User logged off!", HttpStatus.OK); // don't know whether control comes to this line or not!

	}



}
