package com.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/online-bookshop")
public class HomePageController {
	public HomePageController() {
		System.out.println("in ctor of "+getClass().getName());
	}
	
//	public ResponseEntity<?> authenticateCustomer(@RequestBody LoginRequest request) {
//		System.out.println("in auth " + request);
//		// API : o.s.http.ResponseEntity<T> (T body,HttpStatus stsCode)
//		return new ResponseEntity<>(
//				customerService.authenticateCustomer(request.getCustomerId(), request.getPassword()), HttpStatus.OK);
//
//	}
	
	@GetMapping
	public ResponseEntity<?> showHomePage()
	{
		System.out.println("in show Home Page ");
		return new ResponseEntity<>("Welcome to our Web Application", HttpStatus.OK);
	}
}
