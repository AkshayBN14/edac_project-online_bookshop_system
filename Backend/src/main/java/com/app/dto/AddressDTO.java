package com.app.dto;

public class AddressDTO {
//	private int id;
	private String flatNo;
	private String society;
	private String city;
	private String state;
	private String country;
	private int pincode;
//	private User userId;
	
	public AddressDTO() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public AddressDTO(String flatNo, String society, String city, String state, String country, int pincode) {
		super();
		this.flatNo = flatNo;
		this.society = society;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
	}

	public String getFlatNo() {
		return flatNo;
	}

	public void setFlatNo(String flatNo) {
		this.flatNo = flatNo;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society = society;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "AddressDTO [flatNo=" + flatNo + ", society=" + society + ", city=" + city + ", state="
				+ state + ", country=" + country + ", pincode=" + pincode + "]";
	}

	
}








