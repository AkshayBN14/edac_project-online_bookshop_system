package com.app.dto;

import java.util.List;

//TODO

// a JSON cartDTO object =
//{
//	userId: userId,
//	cartBooks: [{
//					bookId: bid, qty: 1, price: 200
//				},
//				{
//					bookId: bid, qty: 1, price: 400	
//				}]
//}

public class CartDTO{
//	private int cartId;
	private int userId;
	private List<CartBookDTO> cartBooks; // set these field of CartBookDTO - bookId, qty, price - from frontend

	public CartDTO() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public CartDTO(int userId, List<CartBookDTO> cartBooks) {
		super();
		this.userId = userId;
		this.cartBooks = cartBooks;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<CartBookDTO> getCartBooks() {
		return cartBooks;
	}

	public void setCartBooks(List<CartBookDTO> cartBooks) {
		this.cartBooks = cartBooks;
	}

	@Override
	public String toString() {
		return "CartDTO [userId=" + userId + ", cartBooks=" + cartBooks + "]";
	}

	
	
}
