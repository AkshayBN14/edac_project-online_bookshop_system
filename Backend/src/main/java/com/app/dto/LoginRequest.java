package com.app.dto;

public class LoginRequest {
//	@JsonProperty(value = "email")
	// userName OR email 
 	private String email;
	private String password;
	
	public LoginRequest() {
		System.out.println("in Login req DTO");
	}

	public LoginRequest(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// REMOVE PASSWORD FORM HERE LATER
	@Override
	public String toString() {
		return "LoginRequest [email=" + email + ", password=" + password + "]";
	}
	
	
	
}
