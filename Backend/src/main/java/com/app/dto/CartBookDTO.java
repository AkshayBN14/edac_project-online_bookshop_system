package com.app.dto;

public class CartBookDTO {
	
	private int bookId; // get bookName and price from bookId to display on frontend
	private int qty; //qty per book
	private double price;
	
	public CartBookDTO() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public CartBookDTO(int bookId, int qty, double price) {
		super();
		this.bookId = bookId;
		this.qty = qty;
		this.price = price;
	}

	
	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "CartBook [BookId: "+ bookId +", qty=" + qty + ", price=" + price + "]";
	}

	

}
