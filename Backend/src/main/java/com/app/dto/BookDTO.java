package com.app.dto;

import com.app.pojos.Category;

public class BookDTO {
//	private int id;
	private String bookName;
	private String author;
	private String description;
	private double price;
	private int copies;
	private Category category;
	//@JsonIgnoreProperties("books") // for preventing stackoverflow
//	private Set<User> users = new HashSet<>(); //read abt set collection
	
	public BookDTO() {
		System.out.println("in POJO: "+getClass().getName());
	}
	public BookDTO(String bookName, String author, String description, double price, int copies, Category category) {
		super();
//		this.id = id;
		this.bookName = bookName;
		this.author = author;
		this.description = description;
		this.price = price;
		this.copies = copies;
		this.category = category;
	}
//	public int getId() {
//		return id;
//	}
//	public void setId(int id) {
//		this.id = id;
//	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getCopies() {
		return copies;
	}
	public void setCopies(int copies) {
		this.copies = copies;
	}
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "BookDTO [bookName=" + bookName + ", author=" + author + ", description=" + description
				+ ", price=" + price + ", copies=" + copies + "]";
	}
	//id=" + id + ",
	
}


















