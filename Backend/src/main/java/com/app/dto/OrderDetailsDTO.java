package com.app.dto;

//TODO
public class OrderDetailsDTO{
	private int id;
//	private Order order;
//	private Book book;
	private int orderedQty;

	public OrderDetailsDTO() {
		System.out.println("in POJO: "+getClass().getName());
	}

	public OrderDetailsDTO(int orderedQty) {
		super();
		this.orderedQty = orderedQty;
	}

	public int getOrderedQty() {
		return orderedQty;
	}

	public void setOrderedQty(int orderedQty) {
		this.orderedQty = orderedQty;
	}

	@Override
	public String toString() {
		return "OrderDetailsDTO [orderedQty=" + orderedQty + "]";
	}
	
	
	
	
}
