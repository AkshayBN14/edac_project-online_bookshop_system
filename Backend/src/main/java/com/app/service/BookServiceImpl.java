package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IBookRepository;
import com.app.dao.IUserRepository;
import com.app.dto.BookDTO;
import com.app.dto.UserDTO;
import com.app.pojos.Book;
import com.app.pojos.Category;
import com.app.pojos.User;

@Service
@Transactional
public class BookServiceImpl implements IBookService {

	// dependency
	@Autowired
	private IBookRepository bookRepo;
	@Autowired
	private IUserRepository customerRepo;

	@Override
	public List<Book> getAllBooks() {
		return bookRepo.findAll();
	}

	@Override
	public List<Book> getBooksByCategory(Category category) {
		return bookRepo.findByCategory(category);
	}

	@Override
	public String addBook(BookDTO bookDTO) { // admin
		if (bookDTO != null) {
			Book book = new Book();
			BeanUtils.copyProperties(bookDTO, book);
			System.out.println("bookDTO : " + bookDTO);
			System.out.println("book : " + book);
			System.out.println("Book added: " + bookRepo.save(book));
			return "Book: " + book.getBookName() + " Added successfully!";
		} else
			return "Book Addition failed!";

	}

	@Override
	public List<Book> getBooksByName(String bName) {
		return bookRepo.findByBookName(bName);
	}

	@Override
	public List<Book> getBooksByPriceLessThan(double bPrice) {
		return bookRepo.findByPriceLessThan(bPrice);
	}

	@Override
	public List<Book> getBooksByAuthor(String bAuthor) {
		return bookRepo.findByAuthor(bAuthor);
	}

	@Override
	public String deleteBookByName(String bName) {
		if (bName != null) {
			bookRepo.deleteByBookName(bName);
			return "Book: " + bName + " deleted successfully!";
		} else
			return "No such book Found with name " + bName + " !";
	}

	@Override
	public String incrementBookCopies(int bookId, int nos) {
		// try to use orElsseThrow below
		try {
			Book book = bookRepo.findById(bookId).get();
			if (book != null) {
				book.setCopies(book.getCopies() + nos);
				return "Book copies for book: " + book.getBookName() + " incremented successfully!";
			}
		} catch (RuntimeException e) {
			e.printStackTrace(); // TODO
		}
		return "Book id: " + bookId + " not found!";
	}

	@Override
	public String decrementBookCopies(int bookId, int nos) {
		// try to use orElsseThrow below
		try {
			Book book = bookRepo.findById(bookId).get();
			if (book != null) {
				book.setCopies(book.getCopies() - nos);
				return "Book copies for book: " + book.getBookName() + " decremented successfully!";
			}
		} catch (RuntimeException e) {
			e.printStackTrace(); // TODO
		}
		return "Book id: " + bookId + " not found!";
	}
	@Override
	public String updateBook(int bookId, BookDTO bookDTO) {
		try {
			Book book = bookRepo.findById(bookId).get();
			if (book != null) {
				System.out.println("book b4: " + book);
				BeanUtils.copyProperties(bookDTO, book, "bookName"); //ignore bookName property
				System.out.println("bookDTO : " + bookDTO);
				System.out.println("book aft: " + book);
				System.out.println("Book added: " + bookRepo.save(book));
				return "Book updated: " + book.getBookName() ;
			}
		} catch (RuntimeException e) {
			e.printStackTrace(); // TODO
		}
		return "Book id: " + bookId + " not found!";
	}

//	@Override
//	public int getBookCopiesByBookId(int bookId) {
//		return bookRepo.getBookCopiesByBookId(bookId);
//	}
//
//	@Override
//	public void updateCopiesAfterPlacingOrder(int newCopiesCount, int bookId) {
//		bookRepo.updateCopiesAfterPlacingOrder(newCopiesCount, bookId);
//	}

}





