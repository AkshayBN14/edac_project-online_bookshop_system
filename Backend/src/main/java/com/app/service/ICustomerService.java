package com.app.service;

import java.util.List;

import com.app.dto.UserDTO;
import com.app.pojos.Book;
import com.app.pojos.Category;
import com.app.pojos.User;

public interface ICustomerService {
	//method to get all users by Id
	List<User> getAllCustomers();
	//method to get all users

	User getCustomersDetails(String email);
	
	String deleteCustomersDetails(User u);

	List<Book> getBooksByCategory(Category category);

	String addCustomer(UserDTO userDTO);
	
}
