package com.app.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.dto.BookDTO;
import com.app.dto.UserDTO;
import com.app.pojos.Book;
import com.app.pojos.Category;
import com.app.pojos.User;

public interface IBookService {
	// method to get all books by Id
	List<Book> getAllBooks(); // whole tables
	// method to get all books

	List<Book> getBooksByCategory(Category category);// cat wise

	String addBook(BookDTO bookDTO); //admin

	List<Book> getBooksByName(String bName);
	
	List<Book> getBooksByPriceLessThan(double bPrice);
	
	List<Book> getBooksByAuthor(String bAuthor);
	
	String deleteBookByName(String bName);
	
	String incrementBookCopies(int bookId, int nos);
	String decrementBookCopies(int bookId, int nos);
	
	String updateBook(int bookId, BookDTO bookDTO);
	
//	public int getBookCopiesByBookId(int bookId);
//	public void updateCopiesAfterPlacingOrder(int newCopiesCount, int bookId);
	
}
