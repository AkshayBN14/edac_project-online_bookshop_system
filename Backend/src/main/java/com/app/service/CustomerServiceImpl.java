package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IBookRepository;
import com.app.dao.IUserRepository;
import com.app.dto.BookDTO;
import com.app.dto.UserDTO;
import com.app.pojos.Book;
import com.app.pojos.Category;
import com.app.pojos.Role;
import com.app.pojos.User;

@Service
@Transactional
public class CustomerServiceImpl implements ICustomerService {
	// dependency
	@Autowired
	private IUserRepository userRepo;

	@Autowired
	private IBookRepository bookRepo;

	@Override
	public List<User> getAllCustomers() {
		return userRepo.findByRole(Role.CUSTOMER);
	}

	@Override
	public User getCustomersDetails(String email) {
		return userRepo.findByEmail(email);
	}

	@Override
	public String deleteCustomersDetails(User u) {
		if (u != null) {
			userRepo.delete(u);
			return "User deleted successfully!";
		} else
			return "No user of specified email found!";
	}

	@Override
	public List<Book> getBooksByCategory(Category category) {
		return bookRepo.findByCategory(category);
	}

	@Override
	public String addCustomer(UserDTO userDTO) {
		if (userDTO != null) {
			User user= new User();
			BeanUtils.copyProperties(userDTO, user);
			System.out.println("userDTO : " + userDTO);
			System.out.println("user : " + user);
			//set role of user
			user.setRole(Role.CUSTOMER);
			System.out.println("user : " + user);
			System.out.println("User added: " + userRepo.save(user));
			return "User Added successfully!";
		} else
			return "User Addition failed!";
	}

}
