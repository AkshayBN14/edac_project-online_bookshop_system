package com.app.service;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IBookRepository;
import com.app.dao.IOrderDetailsRepository;
import com.app.dao.IOrderRepository;
import com.app.dao.IUserRepository;
import com.app.dto.CartBookDTO;
import com.app.dto.CartDTO;
import com.app.pojos.Order;
import com.app.pojos.OrderDetails;
import com.app.pojos.OrderStatus;
import com.app.pojos.User;

@Service
@Transactional
public class OrderServiceImpl implements IOrderService {

	@Autowired
	private IBookRepository bookRepo;
//	@Autowired
//	private ICartRepository cartRepo;
	@Autowired
	private IUserRepository userRepo;
	@Autowired
	private IOrderRepository orderRepo;
	@Autowired
	private IOrderDetailsRepository orderDetailsRepo;
	@Autowired
	private HttpSession session;
	@Autowired
	private IBookService bookService; //used a service as a dependency in another service !! fine ??
	
//	{
//		userId: userId,
//		cartBooks: [{
//						bookId: bid, qty: 1, price: 200
//					},
//					{
//						bookId: bid, qty: 1, price: 400	
//					}]
//	}
	
	//use CartDTO 
	@Override
	public Order placeOrder(int userId, CartDTO cartDTO) {
		
		//create a new order instance
		Order currentOrder = new Order();
		
		//get current user details from userId we got from req header
		User user = userRepo.findById(userId).get();
		
		//1. set the user in Order table
		currentOrder.setUser(user); // order -> user
		
		//2. set the allBooksQty
		int allbooksCount=0;
		int totalPrice=0;
		List<CartBookDTO> booksInCart = cartDTO.getCartBooks();
		//iterate the booksInCart list to get the total books count
		for (CartBookDTO cbd : booksInCart) {
			allbooksCount += cbd.getQty();
			totalPrice += cbd.getQty() * cbd.getPrice();
		}
		//set the total book count/qty to allBooksQty in Order table
		currentOrder.setAllBooksQty(allbooksCount);
		//set the total price in Order table
		currentOrder.setTotalPrice(totalPrice);
		
		//3. set status to packed
		currentOrder.setStatus(OrderStatus.PACKED);
		
		orderRepo.save(currentOrder); // saving details in parent table - Order
		
		System.out.println("User: "+user);
		System.out.println("User orders List: "+user.getOrders());
//		//4. save the order in user table as well we want bidirectional access -- check this later
//		user.getOrders().add(currentOrder); // user -> order
		
		for (CartBookDTO cbd : booksInCart) { // we used for loop due to multiple books present in BooksCart
			//5. initialize the OrderDetails table
			//(i) Order, book, qtyPerBook
			OrderDetails currentOrderDetails = new OrderDetails();
			currentOrderDetails.setOrder(currentOrder);
			currentOrderDetails.setBook(bookRepo.getBookById(cbd.getBookId()));
			currentOrderDetails.setQtyPerBook(cbd.getQty());
			
			//decrease the copies count for each book from Book table -- check this later
//			Book book = bookRepo.getBookById(cb.getBook().getId());
//			book.setCopies(book.getCopies()-cb.getQty());
//			bookRepo.save(book);
			// OR
//			int oldCopiesCount = bookRepo.getBookCopiesByBookId((int) cb.getBook().getId());
//			bookRepo.updateCopiesAfterPlacingOrder(oldCopiesCount-cb.getQty(), (int) cb.getBook().getId());
//			bookRepo.save(cb.getBook()); // we want this to work as a merge - updating the existing book 
			//ERROR: org.hibernate.TransientPropertyValueException: object references an unsaved transient instance - save the transient instance before flushing : com.app.pojos.OrderDetails.order -> com.app.pojos.Order
			
			currentOrder.getOrderDetails().add(currentOrderDetails);
			
			orderDetailsRepo.save(currentOrderDetails); // saving details in child table - OrderDetails
			//7. add orderDetails in the list<orderDetails> in Order POJO 
		}
		
//		//6. empty the cart
//		currentUserCart.getBooksCart().clear();
//		System.out.println("currentuserCart after emptying the cart -> "+currentUserCart); //[]
		
		return currentOrder;
	}
	@Override
	public List<Order> getAllPendingOrders(OrderStatus status) {
		return orderRepo.findByStatus(status);
	}


@Override
	public List<Order> getCustomerOrdersById(int userId) {
		return orderRepo.findByUserId(userId);
	}




}
