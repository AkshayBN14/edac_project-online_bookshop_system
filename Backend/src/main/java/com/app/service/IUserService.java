package com.app.service;

import com.app.dto.AddressDTO;
import com.app.dto.UserDTO;
import com.app.pojos.User;

public interface IUserService {
		
	//method to update/edit profile 
	public String updateProfile(int userId, UserDTO userDTO);

	User authenticateUser(String email, String pswd);

	public String registerAsAdmin(UserDTO userDTO);

	public String registerAsCustomer(UserDTO userDTO);

	public String registerAsDeliveryBoy(UserDTO userDTO);

<<<<<<< HEAD:EDAC_Project/src/main/java/com/app/service/IUserService.java
	
        
        
=======
	public String addAddress(int userId, AddressDTO addressDTO);
>>>>>>> 781c3482d9395d96c6f91484ffb1ec7b87a304f7:Backend/src/main/java/com/app/service/IUserService.java
}
