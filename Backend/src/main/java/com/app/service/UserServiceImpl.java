package com.app.service;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.custom_excs.CustomerHandlingException;
import com.app.dao.IAddressRepository;
import com.app.dao.IBookRepository;
import com.app.dao.IUserRepository;
import com.app.dto.AddressDTO;
import com.app.dto.BookDTO;
import com.app.dto.UserDTO;
import com.app.pojos.Address;
import com.app.pojos.Book;
import com.app.pojos.Role;
import com.app.pojos.User;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

	// dependency
	@Autowired
	private IBookRepository bookRepo;
	@Autowired
	private IUserRepository userRepo;
	@Autowired
	private IAddressRepository addrRepo;
	@Autowired
	private HttpSession session;

	@Override
	public String updateProfile(int userId, UserDTO userDTO) {
		try {
			User user = userRepo.findById(userId).get();
			if (user != null) {
				System.out.println("user b4: " + user);
				BeanUtils.copyProperties(userDTO, user, "userName", "email");
				System.out.println("userDTO : " + userDTO);
				System.out.println("user aft: " + user);
				System.out.println("User added: " + userRepo.save(user));
				return "User updated: " + user.getEmail();
			}
		} catch (RuntimeException e) {
			e.printStackTrace(); // TODO
		}
		return "User id: " + userId + " not found!";
	}

	@Override
	public User authenticateUser(String email, String pswd) {
		// ret customer object in case of success or throw custom exc (eg :
		// CustomerHandlingException : un chked exc)
		return userRepo.validateCustomer(email, pswd)
				.orElseThrow(() -> new CustomerHandlingException("Invalid Credentials!!!!"));
	}// rets in case of success DETACHED customer pojo to the caller or throws un
		// checked custom exc

	@Override
	public String registerAsAdmin(UserDTO userDTO) {
		if (userDTO != null) {
			User user = new User();
			BeanUtils.copyProperties(userDTO, user);
			System.out.println("userDTO : " + userDTO);
			System.out.println("user : " + user);
			// set role of user
			user.setRole(Role.ADMIN);
			System.out.println("ADMIN : " + user);
			System.out.println("ADMIN added: " + userRepo.save(user));
			return "ADMIN with " + user.getEmail() + " registered successfully!";
		} else
			return "ADMIN REGISTRATION failed!";
	}

	@Override
	public String registerAsCustomer(UserDTO userDTO) {
		if (userDTO != null) {
			User user = new User();
			BeanUtils.copyProperties(userDTO, user);
			System.out.println("userDTO : " + userDTO);
			System.out.println("user : " + user);
			// set role of user
			user.setRole(Role.CUSTOMER);
			System.out.println("CUSTOMER : " + user);
			System.out.println("CUSTOMER added: " + userRepo.save(user));
			return "CUSTOMER with " + user.getEmail() + " registered successfully!";
		} else
			return "CUSTOMER REGISTRATION failed!";
	}

	@Override
	public String registerAsDeliveryBoy(UserDTO userDTO) {
		if (userDTO != null) {
			User user = new User();
			BeanUtils.copyProperties(userDTO, user);
			System.out.println("userDTO : " + userDTO);
			System.out.println("user : " + user);
			// set role of user
			user.setRole(Role.DELIVERY_BOY);
			System.out.println("DELIVERY_BOY : " + user);
			System.out.println("DELIVERY_BOY added: " + userRepo.save(user));
			return "DELIVERY_BOY with " + user.getEmail() + " registered successfully!";
		} else
			return "DELIVERY_BOY REGISTRATION failed!";
	}

	@Override
	public String addAddress(int userId, AddressDTO addressDTO) {
		
		try {
			User u = userRepo.findById(userId).get();
			System.out.println("User: "+u);
			if (addressDTO != null) {
				Address address = new Address();
				BeanUtils.copyProperties(addressDTO, address);
				//set user for this address by using userId parameter -- IMP
				address.setUser(u);
				addrRepo.save(address);
				System.out.println("addressDTO : " + addressDTO);
				System.out.println("User: "+u);
				System.out.println("address : " + address);
				return "Address for " + address.getUser().getUserName() + " added successfully!";
			}else {
				System.out.println("in else of addAddress! ");
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return "Address addition failed! User not found!";
	}


}
