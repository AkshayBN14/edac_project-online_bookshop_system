//package com.app.service;
//
//import javax.servlet.ServletContext;
//import javax.servlet.http.HttpSession;
//import javax.transaction.Transactional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.app.dao.IBookRepository;
//import com.app.dao.ICartRepository;
//import com.app.dao.IUserRepository;
//
//@Service
//@Transactional
//public class CartServiceImpl implements ICartService {
//
//	@Autowired
//	private IBookRepository bookRepo;
//	@Autowired
//	private ICartRepository cartRepo;
//	@Autowired
//	private IUserRepository userRepo;
//	@Autowired
//	private ServletContext servCtxt;
//	@Autowired
//	private HttpSession session;

	// booksCart
//	private Book book; // get bookName and price from bookId to display on frontend
//	private int qty;
//	private double price;

//	@Override
//	public Cart getCartByUserId(int userId) {
//		return cartRepo.findByUserId(userId);
//	}
//
//	@Override
//	public List<Cart> getAllCarts() {
//		return cartRepo.findAll();
//	}
//
//	@Override
//	public Cart assignCartToUser(int userId) {
//		Cart newCart = new Cart();
//		newCart.setUser(userRepo.findById(userId).get());
//		cartRepo.save(newCart);
//		
//		return newCart;
//	}
//
//	@Override
//	public String addBookToCart(Cart currentUserCart, int bookId) {
//		System.out.println("---");
//		try {
//			Book book = bookRepo.findById(bookId).get();
//			if (book != null) {
//				CartBook cartBook = new CartBook(book, 1, book.getPrice());
//				currentUserCart.getBooksCart().add(cartBook);
//				System.out.println("Cart ---> " + currentUserCart);
//			} else
//				return "Book with given Id not exists!";
//
//			return "Book: " + book.getBookName() + " added to cart successfully - for User: "
//					+ currentUserCart.getUser().getUserName();
//
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			return "Book Addition to cart failed!";
//		}
//	}

//}

////OLD USER
//List<Cart> allCarts = (List<Cart>) servCtxt.getAttribute("allCarts");
//for (Cart c : allCarts) {
//	if (c.getUser().getId() == userId) {
//		currentUserCart = c;
//		CartBook cartBook = new CartBook(book, 1, book.getPrice());
//		c.getBooksCart().add(cartBook);
//		System.out.println("Cart ---> "+c);
//	}
//}
//}else {
////NEW USER i.e. cart=null so create new cart for that user
//Cart newCart = new Cart(user);
//CartBook cartBook = new CartBook(book, 1, book.getPrice());
//newCart.getBooksCart().add(cartBook);
//cartRepo.save(newCart); //new cart entry for this user added
//System.out.println("newCart ---> "+newCart);
//}