package com.app.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer>{
	@Query("select u from User u left outer join fetch u.userBooks where u.role=:rl")
	public List<User> findByRole(@Param("rl") Role role);
	
	public User findByEmail(String email);
	
	public User deleteByEmail(String email);

	@Query("select u from User u where u.email=:em and u.password=:pass")
	Optional<User> validateCustomer(@Param("em") String email, @Param("pass") String pswd);
}
