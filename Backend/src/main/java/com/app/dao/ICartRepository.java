package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Cart;
import com.app.pojos.User;

public interface ICartRepository extends JpaRepository<Cart, Integer>{

	//get cart by userId
	Cart findByUserId(int userId); 
	
//	@Query("select c from Cart c where c.user.id=:id")
	List<Cart> getCartDetailsByUserId(@Param("id") int userId);

	// add to cart
	
	// displayCart

	// placeOrder

	// viewALLOrders
}
