package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.pojos.Book;
import com.app.pojos.Category;

@Repository
public interface IBookRepository extends JpaRepository<Book, Integer>{
	
	//tried w/o using custom query but error so used this instead
	@Query("select b from Book b left outer join fetch b.users where b.category=:cat")
	public List<Book> findByCategory(@Param("cat") Category cat);
	
	@Query("select b from Book b where b.id=:bid")
	public Book getBookById(@Param("bid") int bookId);
	
	public List<Book> findByBookName(String bookName);
	
	public List<Book> findByPriceLessThan(double bPrice);
	
	public List<Book> findByAuthor(String bAuthor);
	
	public String deleteByBookName(String bName);
	
//	@Query("select b.copies from Book b where b.id=:bid")
//	public int getBookCopiesByBookId(@Param("bid") int bookId);
	
//	@Query("update Book b set b.copies=:nc where b.id=:bid")
//	public void updateCopiesAfterPlacingOrder(@Param("nc") int newCopiesCount, @Param("bid") int bookId);
	
	
}
