package com.app.exc_handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.app.custom_excs.BookHandlingException;
import com.app.custom_excs.CustomerHandlingException;
import com.app.dto.ErrorResponse;

@ControllerAdvice // MANDATORY : class level annot to tell SC / MVC appln whatever follows is a
					// CENTRALIZED EXCEPTION HANDLER CLASS
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	// excptn handling method/s
	// can take array of exception classes : put in {}
	@ExceptionHandler(CustomerHandlingException.class)
	public ResponseEntity<ErrorResponse> handleCustomerHandlingException(CustomerHandlingException e) {
		System.out.println("in handle customer exc");
		return new ResponseEntity<>(new ErrorResponse("Invalid Login", e.getMessage()),
				 HttpStatus.UNAUTHORIZED);
	}
	@ExceptionHandler(BookHandlingException.class)
	public ResponseEntity<ErrorResponse> handleBookHandlingException(BookHandlingException e) {
		System.out.println("in handle acct exc");
		return new ResponseEntity<>(new ErrorResponse("No book with specified id",e.getMessage()),
				 HttpStatus.BAD_REQUEST);
	}
	//catch all -- equivalent
//	@ExceptionHandler(Exception.class)
//	public ResponseEntity<ErrorResponse> handleException(Exception e) {
//		System.out.println("in handle any exc");
//		return new ResponseEntity<>(new ErrorResponse("Server side error !!!!",e.getMessage()),
//				 HttpStatus.INTERNAL_SERVER_ERROR);
//	}
}

